#pragma once
#include "Headers.h"
class Entity;

enum CollisionType {
	TOP_BOTTOM, BOTTOM_TOP, LEFT_RIGHT, RIGHT_LEFT, NONE
};
class CollisionController
{
public:
	static CollisionType onCollision(const Entity*, const Entity*);
};

