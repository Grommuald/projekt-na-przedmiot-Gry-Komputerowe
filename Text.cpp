#include "Text.h"
#include "Texture.h"
#include "ShaderUtil.h"
#include "Log.h"

GLuint Text::vertexbuffer = 0;
GLuint Text::uvbuffer = 0;

Texture* Text::texture = nullptr;
GLuint Text::vao;
map<GLchar, Text::Character> Text::Characters;
GLuint Text::textureId;

void Text::initialize(Texture *font_texture)
{
	texture = font_texture;

	glGenBuffers(1, &vertexbuffer);
	glGenBuffers(1, &uvbuffer);
}

void Text::initialize()
{

}

void Text::dispose()
{
	glDeleteBuffers(1, &vertexbuffer);
	glDeleteBuffers(1, &uvbuffer);
}

void Text::show(const string &text, int x, int y, const int &size)
{
	vector<glm::vec2> vertices;
	vector<glm::vec2> uvs;

	int width = size;
	int height = size;

	for (unsigned int i = 0; i < text.length(); ++i) {
		glm::vec2 vertex_up_left = glm::vec2(x + i*width, y + height);
		glm::vec2 vertex_up_right = glm::vec2(x + i*width + width, y + height);
		glm::vec2 vertex_down_right = glm::vec2(x + i*width + width, y);
		glm::vec2 vertex_down_left = glm::vec2(x + i*width, y);

		vertices.push_back(vertex_up_left);
		vertices.push_back(vertex_down_left);
		vertices.push_back(vertex_up_right);

		vertices.push_back(vertex_down_right);
		vertices.push_back(vertex_up_right);
		vertices.push_back(vertex_down_left);

		char character = text[i];
		int atlas_col = (character - ' ') % 16;
		int atlas_row = (character - ' ') / 16;
		//cout << atlas_col << " " << atlas_row << endl;

		float uv_x = atlas_col / 16.0f;
		float uv_y = atlas_row / 16.0f;
		//cout << uv_x << " " << uv_y << endl;

		glm::vec2 uv_up_left = glm::vec2(uv_x , uv_y);
		glm::vec2 uv_up_right = glm::vec2(uv_x + 1.0f / 16.0f, uv_y);
		glm::vec2 uv_down_right = glm::vec2(uv_x + 1.0f / 16.0f, (uv_y + 1.0f / 16.0f));
		glm::vec2 uv_down_left = glm::vec2(uv_x, (uv_y + 1.0f / 16.0f));

		uvs.push_back(uv_up_left);
		uvs.push_back(uv_down_left);
		uvs.push_back(uv_up_right);

		uvs.push_back(uv_down_right);
		uvs.push_back(uv_up_right);
		uvs.push_back(uv_down_left);
	}
	if (text.length() > 0) {
		glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
		glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(glm::vec2), &vertices[0], GL_DYNAMIC_DRAW);

		glBindBuffer(GL_ARRAY_BUFFER, uvbuffer);
		glBufferData(GL_ARRAY_BUFFER, uvs.size() * sizeof(glm::vec2), &uvs[0], GL_DYNAMIC_DRAW);

		ShaderUtil::getTextShader()->bind();

		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, texture->getId());

		ShaderUtil::getTextShader()->setUniform("textureSampler", 0);

		glEnableVertexAttribArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
		glVertexAttribPointer(
			0,                  // attribute
			2,                  // size
			GL_FLOAT,           // type
			GL_FALSE,           // normalized?
			0,                  // stride
			(void*)0            // array buffer offset
		);

		glEnableVertexAttribArray(1);
		glBindBuffer(GL_ARRAY_BUFFER, uvbuffer);
		glVertexAttribPointer(
			1,                                // attribute
			2,                                // size
			GL_FLOAT,                         // type
			GL_FALSE,                         // normalized?
			0,                                // stride
			(void*)0                          // array buffer offset
		);
		glDepthMask(GL_FALSE);
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

		glDrawArrays(GL_TRIANGLES, 0, vertices.size());

		glDepthMask(GL_TRUE);
		glDisable(GL_BLEND);

		glDisableVertexAttribArray(0);
		glDisableVertexAttribArray(1);
	}
	/*
	ShaderUtil::getTextShader()->bind();
	ShaderUtil::getTextShader()->setUniform("textureSampler", 0);
	ShaderUtil::getTextShader()->setUniform("textColor", glm::vec3(1, 1, 0));
	glActiveTexture(GL_TEXTURE0);
	glBindVertexArray(vao);

	// Iterate through all characters
	std::string::const_iterator c;
	for (c = text.begin(); c != text.end(); c++)
	{
		Character ch = Characters[*c];

		GLfloat xpos = x + ch.Bearing.x * size;
		GLfloat ypos = y - (ch.Size.y - ch.Bearing.y) * size;

		GLfloat w = ch.Size.x * size;
		GLfloat h = ch.Size.y * size;
		// Update VBO for each character
		GLfloat vertices[6][4] = {
			{ xpos,     ypos + h,   0.0, 0.0 },
			{ xpos,     ypos,       0.0, 1.0 },
			{ xpos + w, ypos,       1.0, 1.0 },

			{ xpos,     ypos + h,   0.0, 0.0 },
			{ xpos + w, ypos,       1.0, 1.0 },
			{ xpos + w, ypos + h,   1.0, 0.0 }
		};
		// Render glyph texture over quad
		glBindTexture(GL_TEXTURE_2D, ch.TextureID);
		// Update content of VBO memory
		glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
		glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(vertices), vertices); // Be sure to use glBufferSubData and not glBufferData

		glBindBuffer(GL_ARRAY_BUFFER, 0);
		// Render quad
		glDrawArrays(GL_TRIANGLES, 0, 6);
		// Now advance cursors for next glyph (note that advance is number of 1/64 pixels)
		x += (ch.Advance >> 6) * size; // Bitshift by 6 to get value in pixels (2^6 = 64 (divide amount of 1/64th pixels by 64 to get amount of pixels))
	}
	glBindVertexArray(0);
	glBindTexture(GL_TEXTURE_2D, 0);
	*/
}
