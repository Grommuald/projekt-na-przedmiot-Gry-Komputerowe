#include "Enemy.h"
#include "Sprite3D.h"
#include "Time.h"
#include "Room.h"
#include "Text.h"

Enemy::Enemy(const string& model_name, const string& texture_name, const int &x, const int &y) : Entity(model_name, texture_name, "enemy", x, y)
{
	sprite->setScale(0.1f);

	setWidth(0.2f);
	setHeight(0.2f);

	dead = false;
	dynamic = true;

	health = 1.0f;

	speed = 0.5f;
}

Enemy::~Enemy()
{
}
void Enemy::setPath(vector<LocationDirection> path)
{
}
void Enemy::update()
{
	if (health <= 0.0f) {
		dead = true;
	}
}

void Enemy::dealDamage(const float & damage)
{
	health -= damage;
}
