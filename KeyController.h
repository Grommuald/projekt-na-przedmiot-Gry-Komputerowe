#pragma once
#include "Controller.h"

class KeyController :
	public Controller
{
public:
	KeyController(Player*, Room*);
	~KeyController();

	void update() final;
	void draw() final;
};

