#include "CollisionController.h"
#include "Entity.h"

CollisionType CollisionController::onCollision(const Entity* a, const Entity* b)
{
	float w = 0.5 * (a->getWidth() + b->getWidth());
	float h = 0.5 * (a->getHeight() + b->getHeight());
	float dx = (a->getColliderPosition().x + 0.5f * a->getWidth()) - (b->getColliderPosition().x + 0.5f * b->getWidth());
	float dy = (a->getColliderPosition().y + 0.5f * a->getHeight()) - (b->getColliderPosition().y + 0.5f * b->getHeight());

	if (abs(dx) <= w && abs(dy) <= h)
	{
		float wy = w * dy;
		float hx = h * dx;
		if (wy > hx) {
			if (wy > -hx) {
				return CollisionType::TOP_BOTTOM;
			}
			else {
				return CollisionType::LEFT_RIGHT;
			}	
		}
		else {
			if (wy > -hx) {
				return CollisionType::RIGHT_LEFT;
			}
			else {
				return CollisionType::BOTTOM_TOP;
			}	
		}	
	}
	return CollisionType::NONE;
}
