#include "Obstacle.h"
#include "Sprite3D.h"
#include "Room.h"

unsigned short Obstacle::NUMBER_OF_OBSTACLE_MODELS = 4;

Obstacle::Obstacle(const string& obstacleFilename, const int& tile_x, const int& tile_y, const float &x, const float &y) : Entity(obstacleFilename, "stone_obstacle_texture.dds", "obstacle", glm::vec2(x, y), glm::vec2(0, 0))
{
	setPosition(x, y);
	currentTilePosition.x = tile_x;
	currentTilePosition.y = tile_y;

	sprite->setScale(0.1f);
	sprite->setRenderingHeight(0.1f);

	setWidth(0.2f);
	setHeight(0.2f);

	id = "obstacle";
}

Obstacle::Obstacle(const string& obstacleFilename, const int& tile_x, const int& tile_y, const glm::vec2 &position) : Obstacle(obstacleFilename, tile_x, tile_y, position.x, position.y)
{
}

Obstacle::~Obstacle()
{

}