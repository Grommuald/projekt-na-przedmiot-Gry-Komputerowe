#pragma once
#include "Util.h"

class Shader;
class Window;
class Input;
class Sprite3D;
class Mesh;
class Text;
class CountdownTimer;
class ContainerGUI;
class IconValueGUI;
class Player;
class Obstacle;
class RoomController;
class Doors;
class HUD;
class FadingCaption;

class Game
{
public:
	Game(Window*);
	~Game();

	void input();
	void update();
	void render();
private:
	Window* windowHandle;
	RoomController* roomController;
	HUD* hud;

	FadingCaption* fadingCaption;

	Sprite3D* room;

	Player* player;

	ContainerGUI* containerGUI;
	IconValueGUI* iconValueGUI;
	IconValueGUI* iconValueGUI2;
};

