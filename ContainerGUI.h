#pragma once
#include "GUI.h"

class ContainerGUI : public GUI
{
public:
	ContainerGUI(const string&, const glm::vec2&, 
		const int&, const int&, const int&, const int&, const int&, const int&, const int&, const float&, const float&);
	~ContainerGUI();

	void push(const size_t& val);
	void pop(const size_t& val);
	void setSize(const size_t& val);
	void draw();
private:
	size_t number_of_elements;
};

