#pragma once
#include "Headers.h"

class Sprite3D;

enum class LocationDirection {
	North, East, West, South, None
};
struct Tile {
	int x;
	int y;

	static const float TILE_SIZE;
};
enum class TileStatus {
	Visited, Goal, Empty, Obstacle
};

class Entity
{
public:
	Entity();
	Entity(const string& modelFilename, const string& textureFilename, const string&, const int& tile_x, const int& tile_y);
	Entity(const string& modelFilename, const string& textureFilename, const string&, const glm::vec2& position, const glm::vec2& direction);
	virtual ~Entity();

	virtual void draw();
	virtual void update();

	glm::vec2 getPosition() const { return position; }
	glm::vec2 getColliderPosition() const { return colliderPosition; }
	Tile getCurrentTilePosition() const { return currentTilePosition; }

	glm::vec2 getDirection() const { return direction; }
	float getWidth() const { return width; }
	float getHeight() const { return height; }

	string getId() const { return id; }

	void setPosition(const glm::vec2& val);
	void setPosition(const float& x, const float& y);
	void setColliderOffset(const float& x, const float& y);
	void setTilePosition(const int& x, const int& y);

	void setDirection(const glm::vec2& val);
	void setDirection(const float& x, const float& y);

	void setWidth(const float& val) { width = val; }
	void setHeight(const float& val) { height = val; }

	void setId(const string& val) { id = val; }
	void setSprite(Sprite3D* sprite) { this->sprite = sprite; }

	bool hasChangedTilePosition() const;
	virtual void updateTilePosition();
protected:
	Sprite3D* sprite;
	glm::vec2 position;
	glm::vec2 colliderPosition;
	glm::vec2 colliderOffset;
	glm::vec2 oldPosition;
	glm::vec2 direction;

	Tile currentTilePosition;
	Tile previousTilePosition;

	bool _hasChangedTilePosition;
	string id;

	float width;
	float height;

	float scale;
};

