#pragma once
#include "Controller.h"
#include "DungeonController.h"

class RoomController;
class CountdownTimer;
class Doors;

class DoorController : public Controller
{
public:
	DoorController(Player*, Room*, RoomController*);
	~DoorController();

	void draw() final;
	void update() final;

	void setDoorsOpen();
	void setDoorsClosed();

	bool ifDoorsAvailable(const DungeonController::Location&);
	Doors* getTriggeredDoors();

	void setCurrentRoom(Room*);
private:
	RoomController* roomController;
	CountdownTimer* gatesOpeningTimer;
	Doors* triggeredDoors;
	bool doorsOpen = false;
	bool triggerGates = false;
};

