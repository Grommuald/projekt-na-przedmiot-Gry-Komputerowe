#pragma once
#include "Entity.h"

class Obstacle : public Entity
{
public:
	Obstacle(const string& obstacleFilename, const int& tile_x, const int& tile_y,  const float&, const float&);
	Obstacle(const string& obstacleFilename, const int& tile_x, const int& tile_y, const glm::vec2&);
	~Obstacle();

	static unsigned short NUMBER_OF_OBSTACLE_MODELS;
};

