#pragma once
#include "Enemy.h"
#include "Controller.h"

class CountdownTimer;
/*
class AIController
{
public:
	static void update();
	static void setPlayer(Player*);
	static void spawnEnemy(const int&, const int&);
	static void drawEnemies();
	static void setRoomGrid(vector<TileStatus>);

	enum class LocationStatus {
		Start, Blocked, Valid, Invalid, Goal, Unknown
	};
	struct Location {
		int distanceFromStartX;
		int distanceFromStartY;
		vector<LocationDirection> path;
		LocationStatus status;
	};
private:
	static bool onPlayerEnter;
	static bool refreshAI;

	static CountdownTimer* aiRefreshTimer;
	static vector<LocationDirection> findShortestPath(const Tile& currentCoordinates, vector<TileStatus>&);
	static LocationStatus locationStatus(const Location& location, vector<TileStatus>&);
	static Location exploreInDirection(const Location& currentLocation, const LocationDirection& direction, vector<TileStatus>&);

	static vector<Enemy*> enemies;
	static vector<TileStatus> roomGrid;

	static Player* player;
};
*/
class AIController : public Controller {
public:
	AIController(Player*, Room*);
	~AIController();

	void draw() final;
	void update() final;
	bool isEveryEnemyGone();

	enum class LocationStatus {
		Start, Blocked, Valid, Invalid, Goal, Unknown
	};
	struct Location {
		int distanceFromStartX;
		int distanceFromStartY;
		vector<LocationDirection> path;
		LocationStatus status;
	};
private:
	bool playerGotDamaged = false;
	bool onPlayerEnter;
	bool refreshAI;

	CountdownTimer* aiRefreshTimer;
	CountdownTimer* damageTimer;

	vector<LocationDirection> findShortestPath(const Tile& currentCoordinates, vector<TileStatus>&);
	LocationStatus locationStatus(const Location& location, vector<TileStatus>&);
	Location exploreInDirection(const Location& currentLocation, const LocationDirection& direction, vector<TileStatus>&);
};