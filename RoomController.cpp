#include "RoomController.h"
#include "ObstacleController.h"
#include "AIController.h"
#include "Player.h"
#include "ResourceLoader.h"
#include "Obstacle.h"
#include "ShotController.h"
#include "DungeonController.h"
#include "Room.h"
#include "BossDoors.h"
#include "PlainDoors.h"
#include "DoorController.h"
#include "ControllerObserver.h"
#include "BombController.h"
#include "KeyController.h"
#include "FinalKey.h"
#include "Boss.h"
#include "Walker.h"
#include "BossController.h"
#include "ShaderUtil.h"
#include "FadingCaption.h"
#include "Time.h"

const unsigned short RoomController::NUMBER_OF_PATTERNS = 6;

RoomController::RoomController(Player *player) : Controller(player)
{
	playerDeadCaption = new FadingCaption(8000, ResourceLoader::loadTexture("you_died_screen.dds"));
	bossSlainCaption = new FadingCaption(8000, ResourceLoader::loadTexture("boss_slain_screen.dds"));

	controllersObserver = new ControllerObserver();

	dungeonController = new DungeonController(10, player);
	dungeonController->print();

	rooms = new vector<Room*>(DungeonController::GRID_SIZE * DungeonController::GRID_SIZE);
	setCurrentRoom(DungeonController::GRID_SIZE / 2, DungeonController::GRID_SIZE / 2);

	aiController = new AIController(this->player, currentRoom);
	obstacleController = new ObstacleController(this->player, currentRoom);
	shotController = new ShotController(this->player, currentRoom);
	doorController = new DoorController(this->player, currentRoom, this);
	bombController = new BombController(this->player, currentRoom);
	keyController = new KeyController(this->player, currentRoom);
	

	controllersObserver->addController(doorController);
	controllersObserver->addController(aiController);
	controllersObserver->addController(obstacleController);
	controllersObserver->addController(shotController);
	controllersObserver->addController(bombController);
	controllersObserver->addController(keyController);
	controllersObserver->addController(dungeonController);

	player->setShotController(shotController);
	player->setBombController(bombController);

	player->setDungeonX(DungeonController::GRID_SIZE / 2);
	player->setDungeonY(DungeonController::GRID_SIZE / 2);
	player->setTilePosition(9, 4);
}

RoomController::~RoomController()
{
	delete playerDeadCaption;
	delete bossSlainCaption;

	delete aiController;
	delete obstacleController;
	delete shotController;
	delete doorController;
	delete bombController;
	delete keyController;
}

void RoomController::update()
{
	if (!getEndCondition()) {
		player->update();

		if (aiController->isEveryEnemyGone()) {
			doorController->setDoorsOpen();
		}
		controllersObserver->updateControllers();

		if (bossController) {
			if (!bossController->isBossSlain()) {
				bossController->update();
			}
		}
	}
}

void RoomController::draw()
{
	aiController->draw();
	obstacleController->draw();
	shotController->draw();
	bombController->draw();
	doorController->draw();
	keyController->draw();
	if (bossController) {
		bossController->draw();
	}
	if (getEndCondition()) {
		drawFadingScreen();
	}
}

void RoomController::drawFadingScreen()
{
	if (player->isPlayerDead()) {
		playerDeadCaption->draw();
	}
	else if (bossController->isBossSlain()) {
		bossSlainCaption->draw();
	}
}

void RoomController::addDoors(Room *room)
{
	DungeonController::Location boss_location = DungeonController::Location::None;
	vector<DungeonController::DoorLocation> neighbours = 
		dungeonController->getRoomsNeighbours(room->getDungeonX(), room->getDungeonY());

	for (auto i : neighbours) {
		switch (i.location) {
		case DungeonController::Location::Left:
			if (i.roomType == DungeonController::RoomType::BOSS_ROOM) {
				room->addDoors(new BossDoors(DungeonController::Location::Left));
			} else {
				room->addDoors(new PlainDoors(DungeonController::Location::Left));
			}
			break;
		case DungeonController::Location::Right:
			if (i.roomType == DungeonController::RoomType::BOSS_ROOM) {
				room->addDoors(new BossDoors(DungeonController::Location::Right));
			}
			else {
				room->addDoors(new PlainDoors(DungeonController::Location::Right));
			}
			break;
		case DungeonController::Location::Top:
			if (i.roomType == DungeonController::RoomType::BOSS_ROOM) {
				room->addDoors(new BossDoors(DungeonController::Location::Top));
			}
			else {
				room->addDoors(new PlainDoors(DungeonController::Location::Top));
			}
			break;
		case DungeonController::Location::Bottom:
			if (i.roomType == DungeonController::RoomType::BOSS_ROOM) {
				room->addDoors(new BossDoors(DungeonController::Location::Bottom));
			}
			else {
				room->addDoors(new PlainDoors(DungeonController::Location::Bottom));
			}
			break;
		}
	}
}

void RoomController::setCurrentRoom(const int & dungeon_x, const int & dungeon_y)
{
	if (rooms->at(dungeon_x + dungeon_y * DungeonController::GRID_SIZE) == nullptr) {
		if (doorController) {
			doorController->setDoorsClosed();
			player->setCollidesWithDoors(false);
		}
		switch (dungeonController->getRoomType(dungeon_x, dungeon_y)) {
		case DungeonController::RoomType::STARTING_ROOM:
			rooms->at(dungeon_x + dungeon_y * DungeonController::GRID_SIZE) = generateStartingRoom(dungeon_x, dungeon_y);
			break;
		case DungeonController::RoomType::PLAIN_ROOM:
			rooms->at(dungeon_x + dungeon_y * DungeonController::GRID_SIZE) = generateRoom(dungeon_x, dungeon_y);
			break;
		case DungeonController::RoomType::TREASURE_ROOM:
			rooms->at(dungeon_x + dungeon_y * DungeonController::GRID_SIZE) = generateKeyRoom(dungeon_x, dungeon_y);
			break;
		case DungeonController::RoomType::BOSS_ROOM:
			rooms->at(dungeon_x + dungeon_y * DungeonController::GRID_SIZE) = generateBossRoom(dungeon_x, dungeon_y);
			break;
		}
		currentRoom = rooms->at(dungeon_x + dungeon_y * DungeonController::GRID_SIZE);
	}
	else {
		currentRoom = rooms->at(dungeon_x + dungeon_y * DungeonController::GRID_SIZE);
	}
	controllersObserver->notifyControllers(currentRoom);
}

Room * RoomController::generateRoom(const size_t& x, const size_t& y)
{
	Room* newRoom = new Room(x, y);

	srand(static_cast<unsigned>(time(0)));
	unsigned short patternNumber = rand() % NUMBER_OF_PATTERNS + 1;
	string patternFilename = "room_pattern" + to_string(patternNumber) + ".pat";
	cout << patternFilename << endl;

	short* pattern = ResourceLoader::loadRoomPattern(patternFilename);

	addDoors(newRoom);
	for (int i = 0; i < Room::ROOM_HEIGHT; ++i) {
		for (int j = 0; j < Room::ROOM_WIDTH; ++j) {
			if (pattern[j + i*Room::ROOM_WIDTH] == 1) {
				short obstacleModelNumber = rand() % Obstacle::NUMBER_OF_OBSTACLE_MODELS + 1;
				string obstacleFilename = "rock" + to_string(obstacleModelNumber) + ".obj";

				newRoom->addObstacle(new Obstacle(obstacleFilename, j, i, Room::ROOM_START_X + j * 0.2f, Room::ROOM_START_Y - i * 0.2f - 0.02f));
				newRoom->addToObstaclesGrid(TileStatus::Obstacle);
				newRoom->addToObstaclesCoords(Tile{ j, i });
			}
			else if (pattern[j + i*Room::ROOM_WIDTH] == 2) {
				if (rand() % 2 == 1) {
					newRoom->addEnemy(new Walker(j, i));
				}
				newRoom->addToObstaclesGrid(TileStatus::Empty);
			}
			else {
				newRoom->addToObstaclesGrid(TileStatus::Empty);
			}
		}

	}
	delete pattern;
	return newRoom;
}

Room * RoomController::generateStartingRoom(const size_t & x, const size_t & y)
{
	Room* newRoom = new Room(x, y);
	addDoors(newRoom);
	for (int i = 0; i < Room::ROOM_HEIGHT; ++i) {
		for (int j = 0; j < Room::ROOM_WIDTH; ++j) {
			newRoom->addToObstaclesGrid(TileStatus::Empty);
		}
	}
	return newRoom;
}

Room * RoomController::generateKeyRoom(const size_t & x, const size_t & y)
{
	Room* newRoom = new Room(x, y);
	addDoors(newRoom);
	for (int i = 0; i < Room::ROOM_HEIGHT; ++i) {
		for (int j = 0; j < Room::ROOM_WIDTH; ++j) {
			newRoom->addToObstaclesGrid(TileStatus::Empty);
		}
	}
	newRoom->addKey(new FinalKey(Room::ROOM_WIDTH / 2, Room::ROOM_HEIGHT / 2));
	return newRoom;
}

Room * RoomController::generateBossRoom(const size_t & x, const size_t & y)
{
	Room* newRoom = new Room(x, y);
	addDoors(newRoom);
	for (int i = 0; i < Room::ROOM_HEIGHT; ++i) {
		for (int j = 0; j < Room::ROOM_WIDTH; ++j) {
			newRoom->addToObstaclesGrid(TileStatus::Empty);
		}
	}
	newRoom->addEnemy(new Boss(Room::ROOM_WIDTH / 2, Room::ROOM_HEIGHT / 2));
	bossController = new BossController(player, dynamic_cast<Boss*>(newRoom->getEnemies()->at(0)), newRoom);

	return newRoom;
}

bool RoomController::getEndCondition() const
{
	return player->isPlayerDead() || (bossController != nullptr && bossController->isBossSlain());
}

void RoomController::restart()
{
	for (auto i : *rooms) {
		delete i;
	}
	rooms->clear();
}
