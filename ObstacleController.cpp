#include "ObstacleController.h"
#include "CollisionController.h"
#include "Obstacle.h"
#include "Player.h"
#include "ResourceLoader.h"
#include "Room.h"

/*
vector<TileStatus> ObstacleController::obstaclesGrid;

vector<Obstacle*> ObstacleController::obstacles;
Player* ObstacleController::player;
const unsigned short ObstacleController::NUMBER_OF_PATTERNS = 7;

void ObstacleController::spawnObstacle(const float &x, const float &y)
{
	ObstacleController::spawnObstacle(glm::vec2(x, y));
}

void ObstacleController::spawnObstacle(const glm::vec2 & position)
{
	short obstacleModelNumber = rand() % Obstacle::NUMBER_OF_OBSTACLE_MODELS + 1;
	string obstacleFilename = "rock" + to_string(obstacleModelNumber) + ".obj";

	obstacles.push_back(new Obstacle(obstacleFilename, position));
}

void ObstacleController::generateObstacles()
{
	srand(static_cast<unsigned>(time(0)));
	unsigned short patternNumber = 5;//rand() % NUMBER_OF_PATTERNS + 1;
	string patternFilename = "room_pattern" + to_string(patternNumber) + ".pat";

	short* pattern = ResourceLoader::loadRoomPattern(patternFilename);

	for (int i = 0; i < Room::ROOM_HEIGHT; ++i) {
		for (int j = 0; j < Room::ROOM_WIDTH; ++j) {
			if (pattern[j + i*Room::ROOM_WIDTH] == 1) {
				spawnObstacle(Room::ROOM_START_X + j * 0.2f, Room::ROOM_START_Y - i * 0.2f);
				obstaclesGrid.push_back(TileStatus::Obstacle);
			}
			else {
				obstaclesGrid.push_back(TileStatus::Empty);
			}
		}
	
	}
	delete pattern;
}

void ObstacleController::updateObstacles()
{
	auto i = obstacles.begin();
	while (i != obstacles.end()) {
		CollisionType collisionType = CollisionController::onCollision(*i, player);
		if (collisionType == TOP_BOTTOM) {
			player->setPosition(player->getPosition().x, (*i)->getPosition().y - (*i)->getHeight());
		}
		else if (collisionType == BOTTOM_TOP) {
			player->setPosition(player->getPosition().x, (*i)->getPosition().y + (*i)->getHeight());
		}
		else if (collisionType == LEFT_RIGHT) {
			player->setPosition((*i)->getPosition().x + (*i)->getWidth(), player->getPosition().y);
		}
		else if (collisionType == RIGHT_LEFT) {
			player->setPosition((*i)->getPosition().x - (*i)->getWidth(), player->getPosition().y);
		}
		++i;
	}
}

void ObstacleController::drawObstacles()
{
	for (auto i : obstacles) {
		i->draw();
	}
}

void ObstacleController::disposeObstacles()
{
	auto i = obstacles.begin();
	while (i != obstacles.end()) {
		delete *i;
		i = obstacles.erase(i);
	}
}

vector<TileStatus> ObstacleController::getObstaclesGrid()
{
	return obstaclesGrid;
}

void ObstacleController::setPlayer(Player * p)
{
	player = p;
}
*/

ObstacleController::ObstacleController(Player *player, Room *currentRoom) : Controller(player, currentRoom)
{
}

ObstacleController::~ObstacleController()
{
}
/*
void ObstacleController::spawnObstacle(const float &x, const float &y)
{
	spawnObstacle(glm::vec2(x, y));
}

void ObstacleController::spawnObstacle(const glm::vec2 & position)
{
	short obstacleModelNumber = rand() % Obstacle::NUMBER_OF_OBSTACLE_MODELS + 1;
	string obstacleFilename = "rock" + to_string(obstacleModelNumber) + ".obj";

	currentRoom->getObstacles()->push_back(new Obstacle(obstacleFilename, position));
}
*/
void ObstacleController::update()
{
	auto i = currentRoom->getObstacles()->begin();
	while (i != currentRoom->getObstacles()->end()) {
		CollisionType collisionType = CollisionController::onCollision(*i, player);
		if (collisionType == TOP_BOTTOM) {
			player->setPosition(player->getPosition().x, (*i)->getPosition().y - (*i)->getHeight());
			player->updateTilePosition();
		}
		else if (collisionType == BOTTOM_TOP) {
			player->setPosition(player->getPosition().x, (*i)->getPosition().y + (*i)->getHeight());
			player->updateTilePosition();
		}
		else if (collisionType == LEFT_RIGHT) {
			player->setPosition((*i)->getPosition().x + (*i)->getWidth(), player->getPosition().y);
			player->updateTilePosition();
		}
		else if (collisionType == RIGHT_LEFT) {
			player->setPosition((*i)->getPosition().x - (*i)->getWidth(), player->getPosition().y);
			player->updateTilePosition();
		}
		++i;
	}
}

void ObstacleController::draw()
{
	for (auto i : *(currentRoom->getObstacles())) {
		i->draw();
	}
}
