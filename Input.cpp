#include "Input.h"
#include <algorithm>

const int Input::NUM_KEYCODES = 348;

Key* Input::keys = new Key[NUM_KEYCODES];

void Input::ifKeyPressed(const int &key, std::function<void()> trigger)
{
	if (keys[key].state == KeyState::PRESSED || keys[key].state == KeyState::DOWN) {
		if (trigger) {
			trigger();
		}
	}
}

void Input::ifKeyDown(const int &key, std::function<void()> trigger)
{
	if (keys[key].state == KeyState::DOWN) {
		if (trigger) {
			trigger();
		}
		Input::resetKey(key);
	}
}

void Input::ifKeyUp(const int &key, std::function<void()> trigger)
{
	if (keys[key].state == KeyState::UP) {
		if (trigger) {
			trigger();
		}
		Input::resetKey(key);
	}
}

void Input::resetKey(const int &key)
{
	Input::keys[key].state = KeyState::NONE;
}

void Input::initialize()
{
	for (int i = 0; i < NUM_KEYCODES; ++i) {
		Input::keys[i].keyId = i;
		Input::keys[i].state = KeyState::NONE;
	}
}
void Input::key_callback(GLFWwindow* window, int key, int scancode, int action, int mods) {
	for (int i = 0; i < Input::NUM_KEYCODES; ++i) {
		if (key == i) {
			if (action == GLFW_RELEASE) {
				Input::keys[i].state = KeyState::UP;
			} else if (action == GLFW_REPEAT) {
				Input::keys[i].state = KeyState::DOWN;
			} else if (action == GLFW_PRESS) {
				Input::keys[i].state = KeyState::PRESSED;
			}
		}
	}
}
