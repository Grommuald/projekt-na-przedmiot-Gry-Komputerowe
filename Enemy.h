#pragma once
#include "Entity.h"

class CountdownTimer;

class Enemy :
	public Entity
{
public:
	~Enemy();

	bool isDynamic() const { return dynamic; }
	bool isDead() const { return dead; }
	virtual void setPath(vector<LocationDirection> path);
	virtual void update();

	void dealDamage(const float&);

	void setHealth(const float& health) { this->health = health; }
	float getHealth() const { return health; }
protected:
	Enemy(const string&, const string&, const int&, const int&);

	float old_x, old_y;

	float health;
	float speed = 0.5f;
	bool dynamic;

	bool dead;
};

