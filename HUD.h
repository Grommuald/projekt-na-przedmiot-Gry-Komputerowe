#pragma once
#include "ContainerGUI.h"

class Player;

class HUD
{
public:
	HUD(Player* player, const int& window_width, const int& window_height, const int& number_of_hearts = 3, const bool& keyFound = false);
	~HUD();

	void setHeartsNumber(const int& val);
	void setKeyFound(const bool& val);

	void draw();
private:
	Player* player;

	ContainerGUI* hearts;
	ContainerGUI* key;

	int heartsNumber;
	bool keyFound = false;
};

