#include "KeyController.h"
#include "Room.h"
#include "FinalKey.h"
#include "CollisionController.h"
#include "Player.h"

KeyController::KeyController(Player *player, Room *currentRoom) : Controller(player, currentRoom)
{
}

KeyController::~KeyController()
{
}

void KeyController::update()
{
	if (currentRoom->getKey()) {
		CollisionType collisionType = CollisionController::onCollision(currentRoom->getKey(), player);
		if (collisionType != CollisionType::NONE) {
			player->setKeyObtained(true);
			player->updateHUDKey();
			currentRoom->destroyKey();
		}
	}
}

void KeyController::draw()
{
	if (currentRoom->getKey()) {
		currentRoom->getKey()->draw();
	}
}
