#include "Mesh.h"
#include "Log.h"
#include "Util.h"
#include "ShaderUtil.h"
#include "Transform.h"

Mesh::Mesh()
{
}

Mesh::~Mesh()
{
	glDeleteBuffers(1, &vertexbuffer);
	if (hasNormals) {
		glDeleteBuffers(1, &normalbuffer);
	}
	if (hasTexcoords) {
		glDeleteBuffers(1, &uvbuffer);
	}
}

void Mesh::init()
{
	glGenBuffers(1, &vertexbuffer);
	glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
	glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(glm::vec3), &vertices[0], GL_STATIC_DRAW);

	if (hasTexcoords) {
		glGenBuffers(1, &uvbuffer);
		glBindBuffer(GL_ARRAY_BUFFER, uvbuffer);
		glBufferData(GL_ARRAY_BUFFER, texcoords.size() * sizeof(glm::vec2), &texcoords[0], GL_STATIC_DRAW);
	}
	if (hasNormals) {
		glGenBuffers(1, &normalbuffer);
		glBindBuffer(GL_ARRAY_BUFFER, normalbuffer);
		glBufferData(GL_ARRAY_BUFFER, normals.size() * sizeof(glm::vec3), &normals[0], GL_STATIC_DRAW);
	}
}

void Mesh::draw()
{
	ShaderUtil::getVertexShader()->bind();
	
	glEnableVertexAttribArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
	glVertexAttribPointer(
		0,                  // attribute
		3,                  // size
		GL_FLOAT,           // type
		GL_FALSE,           // normalized?
		0,                  // stride
		(void*)0            // array buffer offset
	);


	if (hasTexcoords) {
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, texture->getId());

		glEnableVertexAttribArray(1);
		glBindBuffer(GL_ARRAY_BUFFER, uvbuffer);
		glVertexAttribPointer(
			1,                                // attribute
			2,                                // size
			GL_FLOAT,                         // type
			GL_FALSE,                         // normalized?
			0,                                // stride
			(void*)0                          // array buffer offset
		);
		ShaderUtil::getVertexShader()->setUniform("textureSampler", 0);
	}
	if (hasNormals) {
		glEnableVertexAttribArray(2);
		glBindBuffer(GL_ARRAY_BUFFER, normalbuffer);
		glVertexAttribPointer(
			2,                                // attribute
			3,                                // size
			GL_FLOAT,                         // type
			GL_FALSE,                         // normalized?
			0,                                // stride
			(void*)0                          // array buffer offset
		);
	}
	
	Transform::setScale(scale, scale, scale);
	Transform::setRotation(rotation);
	Transform::setTranslation(position.x, position.y, position.z);
	ShaderUtil::getVertexShader()->setUniform("MVP", Transform::getProjectedTransformation());
	
	glDrawArrays(GL_TRIANGLES, 0, vertices.size());

	glDisableVertexAttribArray(0);
	if (hasTexcoords) {
		glDisableVertexAttribArray(1);
	}
	if (hasNormals) {
		glDisableVertexAttribArray(2);
	}
}
