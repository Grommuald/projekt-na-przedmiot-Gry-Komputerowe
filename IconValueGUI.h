#pragma once
#include "GUI.h"
class IconValueGUI :
	public GUI
{
public:
	IconValueGUI(const string&, const glm::vec2&,
		const int&, const int&, const int&, const int&, const int&, const int&, const float&, const float&);
	~IconValueGUI();

	string getText() const;
	void setText(const string&);
	void draw();
private:
	string text;
};

