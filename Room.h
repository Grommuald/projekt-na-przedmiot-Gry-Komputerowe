#pragma once
#include "Headers.h"
#include "Entity.h"

class Enemy;
class Obstacle;
class FinalKey;
class Doors;

class Room
{
public:
	Room();
	Room(const size_t& dungeon_x, const size_t& dungeon_y);
	~Room();

	static const short ROOM_WIDTH;
	static const short ROOM_HEIGHT;
	static const float ROOM_START_X;
	static const float ROOM_START_Y;
	static const float ROOM_END_X;
	static const float ROOM_END_Y;

	void updateRoomState();

	vector<Tile>* getObstaclesCoords() {
		return &obstaclesCoords;
	}
	vector<TileStatus>* getObstaclesGrid() {
		return &obstaclesGrid;
	}
	vector<Enemy*>* getEnemies() {
		return enemies;
	}
	vector<Obstacle*>* getObstacles() {
		return obstacles;
	}
	FinalKey* getKey() { return key; }
	vector<Doors*>* getDoors() {
		return doors;
	}
	void addToObstaclesGrid(const TileStatus&);
	void addToObstaclesCoords(const Tile&);
	void addObstacle(Obstacle*);
	void addEnemy(Enemy*);
	void addKey(FinalKey*);
	void addDoors(Doors*);

	void deleteObstacle(const int& tile_x, const int& tile_y);
	void destroyKey();

	int getDungeonX() const { return dungeon_x; }
	void setDungeonX(const int& val) { dungeon_x = val; }
	int getDungeonY() const { return dungeon_y; }
	void setDungeonY(const int& val) { dungeon_y = val; }
private:
	int dungeon_x;
	int dungeon_y;

	Room* leftNeighbour;
	Room* rightNeighbour;
	Room* topNeighbour;
	Room* bottomNeighbour;

	vector<TileStatus> obstaclesGrid;
	vector<Tile> obstaclesCoords;

	vector<Enemy*>* enemies;
	vector<Obstacle*>* obstacles;
	FinalKey* key;
	vector<Doors*>* doors;
};

