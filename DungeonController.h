#pragma once
#include "Controller.h"

class RoomController;

class DungeonController : public Controller
{
public:
	enum class Location {
		Left, Right, Top, Bottom, None
	};
	enum class RoomType {
		STARTING_ROOM,
		TREASURE_ROOM,
		BOSS_ROOM,
		PLAIN_ROOM,
		EMPTY_SPACE
	};
	struct DoorLocation {
		Location location;
		RoomType roomType;
	};
	DungeonController(const size_t&, Player*);
	~DungeonController();

	void generateDungeon();
	void draw() final;
	void update() final;
	void print();

	glm::vec2 getBossRoomPosition() const { return bossRoomPosition; }

	vector<DoorLocation> getRoomsNeighbours(const size_t& x, const size_t& y);
	RoomType getRoomType(const int& x, const int& y) const;
	static const size_t GRID_SIZE;
private:
	glm::vec2 bossRoomPosition;

	size_t numberOfAddedRooms = 0; 
	size_t numberOfRooms;

	vector<RoomType> dungeon;
	RoomController* roomController;
};

