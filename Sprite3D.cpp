#include "Sprite3D.h"
#include "Mesh.h"
#include "ResourceLoader.h"

Sprite3D::Sprite3D()
{

}


Sprite3D::Sprite3D(const string & modelName, const string & textureName)
{
	mesh = ResourceLoader::loadMesh(modelName);
	mesh->setTexture(ResourceLoader::loadTexture(textureName));
	mesh->init();

	setScale(1);
	setPosition(glm::vec2(0.0f, 0.0f));
}

Sprite3D::~Sprite3D()
{
	delete mesh;
}

glm::vec2 Sprite3D::getPosition() const
{
	return position;
}

void Sprite3D::setPosition(const glm::vec2 &val)
{
	position = val;
	mesh->setPosition(glm::vec3(position.x, position.y, renderingHeight));
}

void Sprite3D::setPosition(const float & x, const float & y)
{
	position = glm::vec2(x, y);
	mesh->setPosition(glm::vec3(position.x, position.y, renderingHeight));
}

glm::vec2 Sprite3D::getVelocity() const
{
	return velocity;
}

void Sprite3D::setVelocity(const glm::vec2 &v)
{
	velocity = v;
}

void Sprite3D::setVelocity(const float & x, const float & y)
{
	velocity = glm::vec2(x, y);
}

float Sprite3D::getRenderingHeight() const
{
	return renderingHeight;
}

void Sprite3D::setRenderingHeight(const float &height)
{
	renderingHeight = height;
	mesh->setPosition(glm::vec3(position.x, position.y, height));
}

void Sprite3D::setMesh(Mesh *mesh)
{
	this->mesh = mesh;
}

void Sprite3D::draw()
{
	mesh->draw();
}

void Sprite3D::setRotation(const glm::vec3& rotation, const float &degree)
{
	mesh->setRotation(glm::vec4(rotation.x, rotation.y, rotation.z, glm::radians(degree)));
}

void Sprite3D::setScale(const float &scale)
{
	mesh->setScale(scale);
}
