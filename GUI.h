#pragma once
#include "Headers.h"
class Texture;

class GUI
{
public:
	virtual ~GUI();

	virtual void draw();
	void setPosition(const glm::vec2&);
	void setPosition(const int&, const int&);

protected:
	GUI(const glm::vec2& position, 
		const float& texture_width, const float& texture_height,
		const float& element_width, const float& element_height,
		const float& uv_x, const float& uv_y,
		const float& uv_width, const float& uv_height);

	GLuint vertexbuffer;
	GLuint uvbuffer;

	Texture* texture;

	vector<glm::vec2> vertices;
	vector<glm::vec2> uvs;

	glm::vec2 position;

	float element_width;
	float element_height;
	float texture_width;
	float texture_height;
	float uv_width;
	float uv_height;
	float uv_x;
	float uv_y;
};
