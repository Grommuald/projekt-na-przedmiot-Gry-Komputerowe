#include "Time.h"
#include "Headers.h"

long long Time::getTime()
{
	return glfwGetTime() * Time::Second;
}

double Time::getDelta()
{
	return Time::delta;
}
void Time::setDelta(const double &delta)
{
	Time::delta = delta;
}

const long long Time::Second = 1000000000L;
const long long Time::Millisecond = 1000000L;

double Time::delta = 0.0;

CountdownTimer::CountdownTimer()
{
	elapsedTime = 0LL;
	_hasBeenSet = false;
	_stopTimer = false;
}

void CountdownTimer::start(const long & seconds, function<void()> trigger)
{
	if (!_stopTimer) {
		if (!_hasBeenSet) {
			_hasBeenSet = true;

			elapsedTime = Time::getTime();
		}
		else if (Time::getTime() >= elapsedTime + seconds*Time::Millisecond) {
			trigger();
			_stopTimer = true;
		}
		currentMillisecond = (elapsedTime + seconds*Time::Millisecond - Time::getTime()) / Time::Millisecond;
	}
}

void CountdownTimer::reset()
{
	elapsedTime = 0LL;
	_hasBeenSet = false;
	_stopTimer = false;
}

