#include "Entity.h"
#include "Sprite3D.h"
#include "Room.h"

const float Tile::TILE_SIZE = 0.2f;

Entity::Entity()
{
}

Entity::Entity(const string& modelFilename, const string& textureFilename, const string& id, const int& tile_x, const int& tile_y) : id(id)
{
	sprite =  new Sprite3D(modelFilename, textureFilename);
	position = glm::vec2(0, 0);
	direction = glm::vec2(0, 0);

	setTilePosition(tile_x, tile_y);

	oldPosition.x = getPosition().x;
	oldPosition.y = getPosition().y;

	currentTilePosition.x = tile_x;
	currentTilePosition.y = tile_y;

	colliderPosition = position;

	previousTilePosition = currentTilePosition;

	_hasChangedTilePosition = false;

	setWidth(0);
	setHeight(0);
}

Entity::Entity(const string & modelFilename, const string & textureFilename, const string &, const glm::vec2 & position, const glm::vec2& direction)
{
	sprite = new Sprite3D(modelFilename, textureFilename);
	this->position = position;
	this->direction = direction;

	setWidth(0);
	setHeight(0);
}


Entity::~Entity()
{
	if (sprite) {
		delete sprite;
	}
}
void Entity::setPosition(const glm::vec2& val) {
	position = val;
	colliderPosition = position + colliderOffset;
	sprite->setPosition(position);
}
void Entity::setPosition(const float& x, const float& y) {
	position = glm::vec2(x, y);
	colliderPosition = position + colliderOffset;
	sprite->setPosition(position);
}
void Entity::setColliderOffset(const float & x, const float & y)
{
	colliderOffset.x = x;
	colliderOffset.y = y;
}
void Entity::setTilePosition(const int & x, const int & y)
{
	setPosition(Room::ROOM_START_X + Tile::TILE_SIZE*x, Room::ROOM_START_Y - Tile::TILE_SIZE*y);
}
void Entity::setDirection(const glm::vec2& val) {
	direction = val;
}
void Entity::setDirection(const float& x, const float& y) {
	direction = glm::vec2(x, y);
}

bool Entity::hasChangedTilePosition() const
{
	return _hasChangedTilePosition;
}

void Entity::updateTilePosition()
{
	_hasChangedTilePosition = false;
	if (abs(oldPosition.x - getPosition().x) > 0.2f) {
		if (oldPosition.x > getPosition().x) {
			currentTilePosition.x--;
		}
		else {
			currentTilePosition.x++;
		}
		_hasChangedTilePosition = true;
		oldPosition.x = getPosition().x;
	}
	if (abs(oldPosition.y - getPosition().y) > 0.2f) {
		if (oldPosition.y > getPosition().y) {
			currentTilePosition.y++;
		}
		else {
			currentTilePosition.y--;
		}
		_hasChangedTilePosition = true;
		oldPosition.y = getPosition().y;
	}
}

void Entity::draw()
{
	sprite->draw();
}

void Entity::update()
{
}
