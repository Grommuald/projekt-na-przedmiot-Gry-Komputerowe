#include "ShotController.h"
#include "Shot.h"
#include "CollisionController.h"
#include "Enemy.h"
#include "Player.h"
#include "Room.h"
#include "Obstacle.h"

ShotController::ShotController(Player *player, Room * currentRoom) : Controller(player, currentRoom)
{

}

ShotController::~ShotController()
{
}

void ShotController::spawnShot(const glm::vec2 & position, const glm::vec2 & direction, const float & speed, const float& damage)
{
	bullets.push_back(new Shot(position, direction, speed, damage));
}

void ShotController::update()
{
	// bullets with obstacles (grid solution)
	auto i = bullets.begin();
	while (i != bullets.end()) {
		for (int j = 0; j < currentRoom->getObstaclesCoords()->size(); ++j) {
			if ((*i)->getCurrentTilePosition().x == currentRoom->getObstaclesCoords()->at(j).x &&
				(*i)->getCurrentTilePosition().y == currentRoom->getObstaclesCoords()->at(j).y) {
				cout << "deleting bullet..." << endl;
				delete *i;
				i = bullets.erase(i);
				break;
			}
		}
		if (i != bullets.end()) {
			++i;
		}
	}
	// bullets with walls and enemies
	i = bullets.begin();
	while (i != bullets.end()) {
		(*i)->update();
		if ((*i)->getPosition().x >= 1.85f) {
			delete *i;
			i = bullets.erase(i);
		}
		else if (((*i)->getPosition().x <= -1.85f)) {
			delete *i;
			i = bullets.erase(i);
		}
		else if (((*i)->getPosition().y >= 2.0f)) {
			delete *i;
			i = bullets.erase(i);
		}
		else if (((*i)->getPosition().y <= 0.1f)) {
			delete *i;
			i = bullets.erase(i);
		}
		else {
			for (auto j : *currentRoom->getEnemies()) {
				if (i != bullets.end()) {
					CollisionType collisionType = CollisionController::onCollision(*i, j);
					if (collisionType != CollisionType::NONE) {
						j->dealDamage(player->getDamage());

						delete *i;
						i = bullets.erase(i);
						break;
					}
				}
				else {
					break;
				}
			}
		}
		if (i != bullets.end()) {
			++i;
		}
	}
}

void ShotController::draw()
{
	for (auto i : bullets) {
		i->draw();
	}
}
