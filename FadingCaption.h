#pragma once
#include "Headers.h"
class Texture;
class CountdownTimer;

class FadingCaption
{
public:
	FadingCaption(const int&, Texture* fade_texture, const int& start_x = 0, const int& end_x = 1280, const int& start_y = 0, const int& end_y = 720);
	~FadingCaption();

	void draw();
private:
	float alpha = 0.5f;
	int millisToFadeIn = 10000;

	CountdownTimer* fadeTimer;

	vector<glm::vec2> vertices;
	vector<glm::vec2> uvs;

	GLuint vertexbuffer;
	GLuint uvbuffer;

	Texture* texture;

	bool countdownStopped = false;
	bool stopFading = true;

	int start_x, end_x;
	int start_y, end_y;
};

