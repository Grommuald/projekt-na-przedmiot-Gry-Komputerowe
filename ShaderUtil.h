#pragma once
#include "Shader.h"

class ShaderUtil
{
public:
	static void initializeShaders();
	static void bindShaders();
	static void disposeShaders();

	static Shader* getVertexShader();
	static Shader* getTextShader();
	static Shader* getFadeShader();
private:
	static Shader* vertexShader;
	static Shader* textShader;
	static Shader* fadeShader;
};

