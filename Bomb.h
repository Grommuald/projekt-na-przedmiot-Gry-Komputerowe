#pragma once
#include "Entity.h"

class CountdownTimer;

class Bomb :
	public Entity
{
public:
	Bomb(const glm::vec2& position, const Tile&);
	~Bomb();

	bool ifExplodes() const { return doesExplode; }

	void update();
	void draw();
	void updateTilePosition();
	int getTileRadius() const { return tileRadius; }

private:
	int tileRadius;
	bool doesExplode;
	CountdownTimer* explosionTimer;
};

