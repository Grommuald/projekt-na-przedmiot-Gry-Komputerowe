#pragma once
#include "Headers.h"

class Controller;
class Room;

class ControllerObserver
{
public:
	ControllerObserver();
	~ControllerObserver();

	void notifyControllers(Room* currentRoom);
	void addController(Controller* controller);
	void updateControllers();
private:
	vector<Controller*> observedControllers;
};

