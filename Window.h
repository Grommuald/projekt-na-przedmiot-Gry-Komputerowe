#pragma once
#include "Util.h"

class Window
{
public:
	Window();
	~Window();

	void createWindow(const int& width, const int& height, const std::string& title);
	void render();
	void dispose();

	bool isCloseRequested();

	int getWindowWidth();
	int getWindowHeight();

	void setWindowWidth(const int&);
	void setWindowHeight(const int&);

	double getMouseX() const;
	double getMouseY() const;

	void setMouse(const double&, const double&);

	std::string getTitle();
	GLFWwindow* getWindowHandle();

private:
	int windowWidth, windowHeight;

	glm::vec2 getWindowSize() const;
	glm::vec2 getMouseCoords() const;

	GLFWwindow* windowHandle;
	std::string windowTitle;
};

