#pragma once
#include "Headers.h"

class Window;
class Game;

class Callbacks
{
public:
	static void setWindowHandle(Window*);
	static void setGameHandle(Game*);

	static void window_size_callback(GLFWwindow*, int, int);
private:
	static Window* windowHandle;
	static Game* gameHandle;
};

