#pragma once
#include "Controller.h"

class Shot;
class Enemy;

class ShotController : public Controller
{
public:
	ShotController(Player*, Room*);
	~ShotController();

	void spawnShot(const glm::vec2& position, const glm::vec2& direction, const float& speed, const float& damage);

	void update() final;
	void draw() final;
private:
	vector<Shot*> bullets;
};

