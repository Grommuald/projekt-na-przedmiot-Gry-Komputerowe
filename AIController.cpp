#include "AIController.h"
#include "Room.h"
#include "Player.h"
#include "Time.h"
#include "CollisionController.h"
#include "HUD.h"

/*
vector<Enemy*> AIController::enemies;
vector<TileStatus> AIController::roomGrid;
Player* AIController::player;
bool AIController::onPlayerEnter = true;
bool AIController::refreshAI = true;
CountdownTimer* AIController::aiRefreshTimer = new CountdownTimer();

void AIController::update()
{
	auto i = enemies.begin();
	if (!refreshAI) {
		aiRefreshTimer->start(1000, [&]() {
			refreshAI = true;
		});
	}
	if (refreshAI) {
		while (i != enemies.end()) {
			if ((*i)->isDead()) {
				delete (*i);
				i = enemies.erase(i);
			}
			else {
				if ((*i)->isDynamic()) {
					vector<TileStatus> newGrid = roomGrid;
					(*i)->setPath(findShortestPath((*i)->getCurrentTilePosition(), newGrid));
				}
			}
			++i;
		}
		aiRefreshTimer->reset();
		refreshAI = false;
	}
	i = enemies.begin();
	while (i != enemies.end()) {
		if ((*i)->isDead()) {
			delete (*i);
			i = enemies.erase(i);
		}
		else {
			(*i)->update();
		}
		++i;
	}
}

void AIController::setPlayer(Player *p)
{
	player = p;
}

void AIController::spawnEnemy(const int &x, const int &y)
{
	enemies.push_back(new Enemy(x, y));
}
vector<LocationDirection> AIController::findShortestPath(const Tile & currentCoordinates, vector<TileStatus>& grid)
{
	int distanceFromStartX = currentCoordinates.x;
	int distanceFromStartY = currentCoordinates.y;

	Location location{
		distanceFromStartX,
		distanceFromStartY,
		{},
		LocationStatus::Start
	};
	queue<Location> queue;
	queue.push(location);

	while (queue.size() > 0) {
		Location currentLocation = queue.front();
		queue.pop();
		vector<LocationDirection> directions{
			LocationDirection::North,
			LocationDirection::East,
			LocationDirection::South,
			LocationDirection::West
		};
		for (auto i : directions) {
			Location newLocation = exploreInDirection(currentLocation, i, grid);
			if (newLocation.status == LocationStatus::Goal) {
				return newLocation.path;
			}
			else if (newLocation.status == LocationStatus::Valid) {
				queue.push(newLocation);
			}
		}
	}
	return vector<LocationDirection>(0);
}

AIController::LocationStatus AIController::locationStatus(const Location & location, vector<TileStatus>& grid)
{
	int dfsx = location.distanceFromStartX;
	int dfsy = location.distanceFromStartY;

	if (location.distanceFromStartX < 0 ||
		location.distanceFromStartX >= Room::ROOM_WIDTH  ||
		location.distanceFromStartY < 0 ||
		location.distanceFromStartY >= Room::ROOM_HEIGHT) {
		return LocationStatus::Invalid;
	}
	else if (dfsy == player->getCurrentTilePosition().y && dfsx == player->getCurrentTilePosition().x) {
		//grid[dfsy][dfsx] == TileStatus::Goal
		return LocationStatus::Goal;
	}
	else if (grid[dfsy*Room::ROOM_WIDTH + dfsx] != TileStatus::Empty) {
		return LocationStatus::Blocked;
	} else {
		return LocationStatus::Valid;
	}
}

AIController::Location AIController::exploreInDirection(const Location & currentLocation, const LocationDirection & direction, vector<TileStatus>& grid)
{
	vector<LocationDirection> newPath = currentLocation.path;
	newPath.push_back(direction);

	int dfsx = currentLocation.distanceFromStartX;
	int dfsy = currentLocation.distanceFromStartY;

	if (direction == LocationDirection::North) {
		dfsy -= 1;
	}
	else if (direction == LocationDirection::East) {
		dfsx += 1;
	}
	else if (direction == LocationDirection::South) {
		dfsy += 1;
	}
	else if (direction == LocationDirection::West) {
		dfsx -= 1;
	}
	Location newLocation{
		dfsx,
		dfsy,
		newPath,
		LocationStatus::Unknown
	};
	newLocation.status = locationStatus(newLocation, grid);

	if (newLocation.status == LocationStatus::Valid) {
		grid[dfsy*Room::ROOM_WIDTH + dfsx] = TileStatus::Visited;
	}
	return newLocation;
}
void AIController::drawEnemies()
{
	for (auto i : enemies) {
		i->draw();
	}
}

void AIController::setRoomGrid(vector<TileStatus> grid)
{
	roomGrid = grid;
}
*/

AIController::AIController(Player *player, Room *currentRoom) : Controller(player, currentRoom)
{
	aiRefreshTimer = new CountdownTimer();
	damageTimer = new CountdownTimer();

	auto i = currentRoom->getEnemies()->begin();
	while (i != currentRoom->getEnemies()->end()) {
		if ((*i)->isDynamic()) {
			vector<TileStatus> newGrid = *(currentRoom->getObstaclesGrid());
			(*i)->setPath(findShortestPath((*i)->getCurrentTilePosition(), newGrid));
		}
		++i;
	}
}

AIController::~AIController()
{
	delete damageTimer;
	delete aiRefreshTimer;
}

void AIController::draw()
{
	for (auto i : *(currentRoom->getEnemies())) {
		i->draw();
	}
}

void AIController::update()
{
	if (!isEveryEnemyGone()) {
		auto i = currentRoom->getEnemies()->begin();
		while (i != currentRoom->getEnemies()->end()) {
			if ((*i)->isDead()) {
				delete (*i);
				i = currentRoom->getEnemies()->erase(i);
			}
			else {
				if (!playerGotDamaged) {
					if (CollisionController::onCollision(player, (*i)) != CollisionType::NONE) {
						damageTimer->reset();
						playerGotDamaged = true;

						player->setHealth(player->getHealth() - 1);
						player->updateHUDHealth();
					}
				}
				else {
					damageTimer->start(1000, [&]() {
						playerGotDamaged = false;
					});
				}
				(*i)->update();
			}
			if (i != currentRoom->getEnemies()->end()) {
				++i;
			}
		}
		if (!refreshAI) {
			aiRefreshTimer->start(1000, [&]() {
				refreshAI = true;
			});
		}
		if (refreshAI) {
			i = currentRoom->getEnemies()->begin();
			while (i != currentRoom->getEnemies()->end()) {
				if ((*i)->isDynamic()) {
					vector<TileStatus> newGrid = *(currentRoom->getObstaclesGrid());
					(*i)->setPath(findShortestPath((*i)->getCurrentTilePosition(), newGrid));
				}
				++i;
			}
			aiRefreshTimer->reset();
			refreshAI = false;
		}
	}
}
bool AIController::isEveryEnemyGone()
{
	return currentRoom->getEnemies()->empty();
}
vector<LocationDirection> AIController::findShortestPath(const Tile & currentCoordinates, vector<TileStatus>& grid)
{
	int distanceFromStartX = currentCoordinates.x;
	int distanceFromStartY = currentCoordinates.y;

	Location location{
		distanceFromStartX,
		distanceFromStartY,
		{},
		LocationStatus::Start
	};
	queue<Location> queue;
	queue.push(location);

	while (queue.size() > 0) {
		Location currentLocation = queue.front();
		queue.pop();
		vector<LocationDirection> directions{
			LocationDirection::North,
			LocationDirection::East,
			LocationDirection::South,
			LocationDirection::West
		};
		for (auto i : directions) {
			Location newLocation = exploreInDirection(currentLocation, i, grid);
			if (newLocation.status == LocationStatus::Goal) {
				return newLocation.path;
			}
			else if (newLocation.status == LocationStatus::Valid) {
				queue.push(newLocation);
			}
		}
	}
	return vector<LocationDirection>(0);
}

AIController::LocationStatus AIController::locationStatus(const Location & location, vector<TileStatus>& grid)
{
	int dfsx = location.distanceFromStartX;
	int dfsy = location.distanceFromStartY;

	if (location.distanceFromStartX < 0 ||
		location.distanceFromStartX >= Room::ROOM_WIDTH ||
		location.distanceFromStartY < 0 ||
		location.distanceFromStartY >= Room::ROOM_HEIGHT) {
		return LocationStatus::Invalid;
	}
	else if (dfsy == player->getCurrentTilePosition().y && dfsx == player->getCurrentTilePosition().x) {
		//grid[dfsy][dfsx] == TileStatus::Goal
		return LocationStatus::Goal;
	}
	else if (grid[dfsy*Room::ROOM_WIDTH + dfsx] != TileStatus::Empty) {
		return LocationStatus::Blocked;
	}
	else {
		return LocationStatus::Valid;
	}
}

AIController::Location AIController::exploreInDirection(const Location & currentLocation, const LocationDirection & direction, vector<TileStatus>& grid)
{
	vector<LocationDirection> newPath = currentLocation.path;
	newPath.push_back(direction);

	int dfsx = currentLocation.distanceFromStartX;
	int dfsy = currentLocation.distanceFromStartY;

	if (direction == LocationDirection::North) {
		dfsy -= 1;
	}
	else if (direction == LocationDirection::East) {
		dfsx += 1;
	}
	else if (direction == LocationDirection::South) {
		dfsy += 1;
	}
	else if (direction == LocationDirection::West) {
		dfsx -= 1;
	}
	Location newLocation{
		dfsx,
		dfsy,
		newPath,
		LocationStatus::Unknown
	};
	newLocation.status = locationStatus(newLocation, grid);

	if (newLocation.status == LocationStatus::Valid) {
		grid[dfsy*Room::ROOM_WIDTH + dfsx] = TileStatus::Visited;
	}
	return newLocation;
}