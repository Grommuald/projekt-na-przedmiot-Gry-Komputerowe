#include "ControllerObserver.h"
#include "Controller.h"


ControllerObserver::ControllerObserver()
{
}


ControllerObserver::~ControllerObserver()
{
}

void ControllerObserver::notifyControllers(Room * currentRoom)
{
	for (auto i : observedControllers) {
		i->setCurrentRoom(currentRoom);
	}
}

void ControllerObserver::addController(Controller * controller)
{
	observedControllers.push_back(controller);
}

void ControllerObserver::updateControllers()
{
	for (auto i : observedControllers) {
		i->update();
	}
}
