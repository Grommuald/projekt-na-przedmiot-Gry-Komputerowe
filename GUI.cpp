#include "GUI.h"
#include "Texture.h"
#include "ShaderUtil.h"


GUI::GUI(const glm::vec2& position,
	const float& texture_width, const float& texture_height,
	const float& element_width, const float& element_height,
	const float& uv_x, const float& uv_y,
	const float& uv_width, const float& uv_height) :
	position(position),
	texture_width(texture_width), texture_height(texture_height), 
	element_width(element_width), element_height(element_height),
	uv_x(uv_x), uv_y(uv_y),
	uv_width(uv_width), uv_height(uv_height)
{
	glGenBuffers(1, &vertexbuffer);
	glGenBuffers(1, &uvbuffer);
}


GUI::~GUI()
{
	glDeleteBuffers(1, &vertexbuffer);
	glDeleteBuffers(1, &uvbuffer);
}

void GUI::draw()
{
	glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
	glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(glm::vec2), &vertices[0], GL_DYNAMIC_DRAW);

	glBindBuffer(GL_ARRAY_BUFFER, uvbuffer);
	glBufferData(GL_ARRAY_BUFFER, uvs.size() * sizeof(glm::vec2), &uvs[0], GL_DYNAMIC_DRAW);

	ShaderUtil::getTextShader()->bind();

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, texture->getId());

	ShaderUtil::getTextShader()->setUniform("textureSampler", 0);

	glEnableVertexAttribArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
	glVertexAttribPointer(
		0,                  // attribute
		2,                  // size
		GL_FLOAT,           // type
		GL_FALSE,           // normalized?
		0,                  // stride
		(void*)0            // array buffer offset
	);

	glEnableVertexAttribArray(1);
	glBindBuffer(GL_ARRAY_BUFFER, uvbuffer);
	glVertexAttribPointer(
		1,                                // attribute
		2,                                // size
		GL_FLOAT,                         // type
		GL_FALSE,                         // normalized?
		0,                                // stride
		(void*)0                          // array buffer offset
	);
	glEnable(GL_BLEND);
	glDepthMask(GL_FALSE);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glDrawArrays(GL_TRIANGLES, 0, vertices.size());

	glDepthMask(GL_TRUE);
	glDisable(GL_BLEND);

	glDisableVertexAttribArray(0);
	glDisableVertexAttribArray(1);
}

void GUI::setPosition(const glm::vec2 &val)
{
	position = val;
}

void GUI::setPosition(const int &x, const int &y)
{
	position = glm::vec2(x, y);
}
