#include "Player.h"
#include "Sprite3D.h"
#include "Input.h"
#include "Time.h"
#include "ShotController.h"
#include "Text.h"
#include "Room.h"
#include "DungeonController.h"
#include "BombController.h"
#include "HUD.h"

Player::Player() : Player(0, 0)
{
	
}

Player::Player(const int &x, const int &y) : Entity("player.obj", "rock.bmp", "player", x, y)
{
	sprite->setScale(0.1f);
	shootingTimer = new CountdownTimer();
	bombPlacementTimer = new CountdownTimer();

	canShoot = true;
	canPlaceBomb = true;

	shotSpeed = 1.0f;
	shootingFrequency = 500;

	setWidth(0.2f);
	setHeight(0.2f);

	healthContainers = 3;

	damage = 0.2f;

	dungeon_x = DungeonController::GRID_SIZE / 2;
	dungeon_y = DungeonController::GRID_SIZE / 2;
}


Player::~Player()
{
	delete shootingTimer;
	delete bombPlacementTimer;
}

void Player::input()
{
	Input::ifKeyPressed(GLFW_KEY_W, [&]() {
		setDirection(0, 1);
		setPosition(getPosition() + getDirection() * (float)Time::getDelta());
	});
	Input::ifKeyPressed(GLFW_KEY_S, [&]() {
		setDirection(0, -1);
		setPosition(getPosition() + getDirection() * (float)Time::getDelta());
	});
	Input::ifKeyPressed(GLFW_KEY_A, [&]() {
		setDirection(-1, 0);
		setPosition(getPosition() + getDirection() * (float)Time::getDelta());
	});
	Input::ifKeyPressed(GLFW_KEY_D, [&]() {
		setDirection(1, 0);
		setPosition(getPosition() + getDirection() * (float) Time::getDelta());
	});

	Input::ifKeyPressed(GLFW_KEY_UP, [&]() {
		if (canShoot) {
			shotController->spawnShot(getPosition(), glm::vec2(0, 1), shotSpeed, damage);

			shootingTimer->reset();
			canShoot = false;
		}
	});
	Input::ifKeyPressed(GLFW_KEY_DOWN, [&]() {
		if (canShoot) {
			shotController->spawnShot(getPosition(), glm::vec2(0, -1), shotSpeed, damage);

			shootingTimer->reset();
			canShoot = false;
		}
	});
	Input::ifKeyPressed(GLFW_KEY_LEFT, [&]() {
		if (canShoot) {
			shotController->spawnShot(getPosition(), glm::vec2(-1, 0), shotSpeed, damage);

			shootingTimer->reset();
			canShoot = false;
		}
	});
	Input::ifKeyPressed(GLFW_KEY_RIGHT, [&]() {
		if (canShoot) {
			shotController->spawnShot(getPosition(), glm::vec2(1, 0), shotSpeed, damage);

			shootingTimer->reset();
			canShoot = false;
		}
	});
	Input::ifKeyPressed(GLFW_KEY_E, [&]() {
		if (canPlaceBomb) {
			bombController->spawnBomb(getPosition(), getCurrentTilePosition());

			bombPlacementTimer->reset();
			canPlaceBomb = false;
		}
	});
}

void Player::update()
{
	if (!collidesWithDoors) {
		if (position.x >= 1.80f) {
			setPosition(1.80f, position.y);
		}
		if (position.x <= -1.80f) {
			setPosition(-1.80f, position.y);
		}
		if (position.y >= 1.85f) {
			setPosition(position.x, 1.85f);
		}
		if (position.y <= 0.2f) {
			setPosition(position.x, 0.2f);
		}

	}
	if (!canShoot) {
		shootingTimer->start(shootingFrequency, [&]() {
			canShoot = true;
		});
	}
	if (!canPlaceBomb) {
		bombPlacementTimer->start(1500, [&]() {
			canPlaceBomb = true;
		});
	}
	updateTilePosition();

	if (healthContainers <= 0) {
		isDead = true;
	}
}

void Player::draw()
{
	Entity::draw();
	
	//Text::show("Player X: " + to_string(position.x) + " (" + to_string(currentTilePosition.x) + ")", 0, 48, 32);
	//Text::show("Player Y: " + to_string(position.y) + " (" + to_string(currentTilePosition.y) + ")", 0, 0, 32);
}

void Player::updateTilePosition()
{
	currentTilePosition.x = static_cast<int>((getPosition().x + 0.1f + 1.80f) / 3.60f * 1000.0f) / 55;
	currentTilePosition.y = Room::ROOM_HEIGHT - static_cast<int>(((getPosition().y - 0.25f) / 1.60f)* 1000.0f) / 125 - 1;
}

void Player::setShotController(ShotController *shotController)
{
	this->shotController = shotController;
}

void Player::setBombController(BombController *bombController)
{
	this->bombController = bombController;
}

void Player::setDungeonPosition(const int & x, const int & y)
{
	dungeon_x = x;
	dungeon_y = y;
}

void Player::updateHUDHealth()
{
	hud->setHeartsNumber(healthContainers);
}

void Player::updateHUDKey()
{
	hud->setKeyFound(true);
}

void Player::setCanShoot(const bool & val)
{
	canShoot = true;
	shootingTimer->reset();
}
