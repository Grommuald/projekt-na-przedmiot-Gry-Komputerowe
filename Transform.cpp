#include "Transform.h"
#include "Log.h"
#include "Camera.h"
#include "ShaderUtil.h"

glm::vec3 Transform::translation = glm::vec3(0.0f, 0.0f, 0.0f);
glm::vec4 Transform::rotation = glm::vec4(0.0f, 0.0f, 0.0f, 0.0f);
glm::vec3 Transform::scale = glm::vec3(1.0f, 1.0f, 1.0f);

float Transform::zNear = 0.0f;
float Transform::zFar = 0.0f;
float Transform::width = 0.0f;
float Transform::height = 0.0f;
float Transform::fov = 0.0f;

glm::mat4 Transform::getTransformation()
{
	glm::mat4 translationMatrix = glm::translate(glm::mat4(1), translation);
	// z musi by� != 0.0f, bo inaczej uzupe�nia warto�ciami -nan 
	glm::mat4 rotationMatrix = 
		glm::rotate(glm::mat4(1), rotation.w, glm::vec3(rotation.x ,rotation.y , rotation.z == 0.0f ? 0.000001f : rotation.z));
	glm::mat4 scaleMatrix = glm::scale(glm::mat4(1), scale);

	return translationMatrix * rotationMatrix * scaleMatrix;
}

glm::vec3 Transform::getTranslation()
{
	return translation;
}

glm::vec4 Transform::getRotation()
{
	return rotation;
}

glm::vec3 Transform::getScale()
{
	return scale;
}

void Transform::setTranslation(const glm::vec3 &val)
{
	translation = val;
}

void Transform::setTranslation(const float &x, const float &y, const float &z)
{
	translation = glm::vec3(x, y, z);
}

void Transform::setRotation(const glm::vec4 &val)
{
	rotation = val;
}

void Transform::setRotation(const float &x, const float &y, const float &z, const float& degree)
{
	rotation = glm::vec4(x, y, z, degree);
}

void Transform::setScale(const glm::vec3& val)
{
	scale = val;
}

void Transform::setScale(const float &x, const float &y, const float &z)
{
	scale = glm::vec3(x, y, z);
}

glm::mat4 Transform::getProjectedTransformation()
{
	glm::mat4 transformationMatrix = getTransformation();
	ShaderUtil::getVertexShader()->setUniform("M", transformationMatrix);
	glm::mat4 projectionMatrix = glm::perspective(fov, width / height, zNear, zFar);
	glm::mat4 viewMatrix = glm::lookAt(
		Camera::getPosition(),           // Camera is here
		Camera::getPosition() + Camera::getDirection(), // and looks here : at the same position, plus "direction"
		Camera::getUp()                  // Head is up (set to 0,-1,0 to look upside-down)
	);
	ShaderUtil::getVertexShader()->setUniform("V", viewMatrix);
	glm::mat4 mvp = projectionMatrix * viewMatrix * transformationMatrix;
	return mvp;
}

void Transform::setProjection(const float &fov, const float &width,
	const float &height, const float &zNear, const float &zFar)
{
	Transform::fov = fov;
	Transform::width = width;
	Transform::height = height;
	Transform::zNear = zNear;
	Transform::zFar = zFar;
}
