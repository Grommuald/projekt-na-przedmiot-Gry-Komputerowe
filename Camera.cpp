#include "Camera.h"
#include "Time.h"
#include "Window.h"
#include "Input.h"

glm::mat4 Camera::modelMatrix;
glm::mat4 Camera::viewMatrix;
glm::mat4 Camera::projectionMatrix;

glm::mat4 Camera::mvp;

glm::vec3 Camera::position;
glm::vec3 Camera::direction;
glm::vec3 Camera::up;
glm::vec3 Camera::right;

float Camera::initialFoV;
float Camera::horizontalAngle;
float Camera::verticalAngle;
float Camera::speed;
float Camera::mouseSpeed;

Window* Camera::windowHandle;


void Camera::initialize(Window *window, glm::vec3 pos)
{
	Camera::windowHandle = window;
	modelMatrix = glm::mat4(1.0f);
	viewMatrix = glm::lookAt(glm::vec3(4, 3, 3), glm::vec3(0, 0, 0), glm::vec3(0, 1, 0));
	projectionMatrix = glm::perspective(70.0f, (float) windowHandle->getWindowWidth() / windowHandle->getWindowHeight(), 0.1f, 1000.0f);

	mvp = projectionMatrix * viewMatrix * modelMatrix;

	position = pos;
	horizontalAngle = 3.14f;
	verticalAngle = 0.0f;

	initialFoV = 45.0f;

	speed = 5.0f;
	mouseSpeed = 0.005f;
}

void Camera::calculateTransformation()
{
	// Get mouse position
	double xpos = windowHandle->getMouseX();
	double ypos = windowHandle->getMouseY();

	windowHandle->setMouse(800 / 2, 600 / 2);

	// Reset mouse position for next frame
	
	// Compute new orientation
	//horizontalAngle += mouseSpeed * float(1024 / 2 - xpos);
	//verticalAngle += mouseSpeed * float(768 / 2 - ypos);

	// Direction : Spherical coordinates to Cartesian coordinates conversion
	direction = glm::vec3(
		cos(verticalAngle) * sin(horizontalAngle),
		sin(verticalAngle),
		cos(verticalAngle) * cos(horizontalAngle)
	);

	// Right vector
	right = glm::vec3(
		sin(horizontalAngle - 3.14f / 2.0f),
		0,
		cos(horizontalAngle - 3.14f / 2.0f)
	);

	// Up vector
	up = glm::cross(right, direction);
	//cout << position.x << " " << position.y << " " << position.z << endl;

	float FoV = initialFoV;// - 5 * glfwGetMouseWheel(); // Now GLFW 3 requires setting up a callback for this. It's a bit too complicated for this beginner's tutorial, so it's disabled instead.

						   // Projection matrix : 45� Field of View, 4:3 ratio, display range : 0.1 unit <-> 100 units
	projectionMatrix = glm::perspective(FoV, 4.0f / 3.0f, 0.1f, 100.0f);
	// Camera matrix
	viewMatrix = glm::lookAt(
		position,           // Camera is here
		position + direction, // and looks here : at the same position, plus "direction"
		up                  // Head is up (set to 0,-1,0 to look upside-down)
	);
	mvp = projectionMatrix * viewMatrix * modelMatrix;
}

void Camera::input()
{
	// Move forward
	Input::ifKeyPressed(GLFW_KEY_W, [&]() {
		position += direction * (float)Time::getDelta() * speed;

	});
	Input::ifKeyPressed(GLFW_KEY_S, [&]() {
		position -= direction * (float)Time::getDelta() * speed;

	});
	Input::ifKeyPressed(GLFW_KEY_D, [&]() {
		position += right * (float)Time::getDelta() * speed;

	});
	Input::ifKeyPressed(GLFW_KEY_A, [&]() {
		position -= right * (float)Time::getDelta() * speed;

	});
}

glm::mat4 Camera::getMVP()
{
	return mvp;
}

glm::mat4 Camera::getModel()
{
	return modelMatrix;
}

glm::mat4 Camera::getProjection()
{
	return projectionMatrix;
}

glm::mat4 Camera::getView()
{
	return viewMatrix;
}

glm::vec3 Camera::getPosition()
{
	return position;
}

glm::vec3 Camera::getUp()
{
	return up;
}

glm::vec3 Camera::getRight()
{
	return right;
}

glm::vec3 Camera::getDirection()
{
	return direction;
}
