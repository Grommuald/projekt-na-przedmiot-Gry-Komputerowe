#include "HUD.h"
#include "Player.h"

HUD::HUD(Player* player, const int& window_width, const int& window_height, const int& number_of_hearts, const bool& keyFound) : player(player)
{
	hearts = new ContainerGUI("heart.dds", glm::vec2(16, 640), heartsNumber, 0, 0, 128, 128, 384, 128, 64, 64);
	key = new ContainerGUI("heart.dds", glm::vec2(16, 540), 1, 256, 0, 128, 128, 384, 128, 64, 64);

	hearts->setSize(player->getHealth());
}


HUD::~HUD()
{
	delete hearts;
	delete key;
}

void HUD::setHeartsNumber(const int & val)
{
	hearts->setSize(val);
}

void HUD::setKeyFound(const bool & val)
{
	keyFound = val;
}

void HUD::draw()
{
	hearts->draw();
	if (keyFound) {
		key->draw();
	}
}
