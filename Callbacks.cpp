#include "Callbacks.h"
#include "Window.h"
#include "Game.h"
#include "Log.h"
#include "Transform.h"
#include "ShaderUtil.h"

Window* Callbacks::windowHandle = nullptr;
Game* Callbacks::gameHandle = nullptr;

void Callbacks::setWindowHandle(Window *window)
{
	Callbacks::windowHandle = window;
}

void Callbacks::setGameHandle(Game *game)
{
	Callbacks::gameHandle = game;
}

void Callbacks::window_size_callback(GLFWwindow *window, int width, int height)
{
	windowHandle->setWindowWidth(width);
	windowHandle->setWindowHeight(height);

	Transform::setProjection(70.0f, width, height, 0.1f, 1000.0f);
	ShaderUtil::getTextShader()->setUniform("screen_width", width);
	ShaderUtil::getTextShader()->setUniform("screen_height", height);
}
