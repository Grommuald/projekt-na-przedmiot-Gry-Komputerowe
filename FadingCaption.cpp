#include "FadingCaption.h"
#include "ShaderUtil.h"
#include "Texture.h"
#include "Time.h"

FadingCaption::FadingCaption(const int& millisToFadeIn, Texture* fade_texture, const int& start_x, const int& end_x,
	const int& start_y, const int& end_y) 
	: millisToFadeIn(millisToFadeIn), start_x(start_x), end_x(end_x), start_y(start_y), end_y(end_y)
{
	texture = fade_texture;
	fadeTimer = new CountdownTimer();

	glGenBuffers(1, &vertexbuffer);
	glGenBuffers(1, &uvbuffer);

	vertices.push_back(glm::vec2(start_x, end_y));
	vertices.push_back(glm::vec2(start_x, start_y));
	vertices.push_back(glm::vec2(end_x, end_y));

	vertices.push_back(glm::vec2(end_x, start_y));
	vertices.push_back(glm::vec2(end_x, end_y));
	vertices.push_back(glm::vec2(start_x, start_y));

	uvs.push_back(glm::vec2(0, 0));
	uvs.push_back(glm::vec2(0, 1));
	uvs.push_back(glm::vec2(1, 0));

	uvs.push_back(glm::vec2(1, 1));
	uvs.push_back(glm::vec2(1, 0));
	uvs.push_back(glm::vec2(0, 1));


}


FadingCaption::~FadingCaption()
{
	glDeleteBuffers(1, &vertexbuffer);
	glDeleteBuffers(1, &uvbuffer);
}

void FadingCaption::draw()
{
	if (!countdownStopped) {
		fadeTimer->start(millisToFadeIn, [&]() {
			countdownStopped = true;
			alpha = 1.0f;
		});
		if (!countdownStopped) {
			alpha = (1.0f - fadeTimer->getCurrentMillisecond() / (float)millisToFadeIn);
		}
	}
	glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
	glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(glm::vec2), &vertices[0], GL_STATIC_DRAW);

	glBindBuffer(GL_ARRAY_BUFFER, uvbuffer);
	glBufferData(GL_ARRAY_BUFFER, uvs.size() * sizeof(glm::vec2), &uvs[0], GL_STATIC_DRAW);

	ShaderUtil::getFadeShader()->bind();

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, texture->getId());

	ShaderUtil::getFadeShader()->setUniform("textureSampler", 0);
	ShaderUtil::getFadeShader()->setUniform("alpha", alpha);

	glEnableVertexAttribArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
	glVertexAttribPointer(
		0,                  // attribute
		2,                  // size
		GL_FLOAT,           // type
		GL_FALSE,           // normalized?
		0,                  // stride
		(void*)0            // array buffer offset
	);

	glEnableVertexAttribArray(1);
	glBindBuffer(GL_ARRAY_BUFFER, uvbuffer);
	glVertexAttribPointer(
		1,                                // attribute
		2,                                // size
		GL_FLOAT,                         // type
		GL_FALSE,                         // normalized?
		0,                                // stride
		(void*)0                          // array buffer offset
	);
	glDepthMask(GL_FALSE);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glDrawArrays(GL_TRIANGLES, 0, vertices.size());

	glDepthMask(GL_TRUE);
	glDisable(GL_BLEND);

	glDisableVertexAttribArray(0);
	glDisableVertexAttribArray(1);
}