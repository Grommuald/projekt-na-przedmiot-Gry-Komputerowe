#include "Engine.h"
#include "Time.h"
#include "Input.h"
#include "Window.h"
#include "Game.h"
#include "Log.h"
#include "ShaderUtil.h"
#include "Callbacks.h"
#include "Text.h"

Engine::Engine()
{
	isRunning = false;

	Log::log("Creating window...");
	window = new Window;
	window->createWindow(Width, Height, Title);
	Log::log("Window created.");

	Log::log("Initializing graphics...");
	initializeGraphics();

	Log::log("Creating game...");
	game = new Game(window);
	
	Log::log("Initializing input...");
	Input::initialize();

	std::string version = (char*)glGetString(GL_VERSION);
	Log::log("Current OpenGL version: " + version);

	Callbacks::setGameHandle(game);
	Callbacks::setWindowHandle(window);

	Text::initialize(ResourceLoader::loadTexture("font.dds"));
	//Text::initialize();
}


Engine::~Engine()
{
	delete window;
	delete game;
}

void Engine::start()
{
	Log::log("Starting engine...");
	if (isRunning) {
		return;
	}
	Log::log("Entering run() method...");
	run();
}

void Engine::stop()
{
	if (!isRunning) {
		return; 
	}
	isRunning = false;
}

void Engine::initializeGraphics()
{
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

	//glEnable(GL_CULL_FACE);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS);
	glEnable(GL_FRAMEBUFFER_SRGB);

	ShaderUtil::initializeShaders();
}

void Engine::run()
{
	isRunning = true;

	int frameCounter = 0;
	int frames = 0;

	const double frameTime = 1.0 / FrameCap;

	long long lastTime = Time::getTime();
	double unprocessedTime = 0.0;

	Log::log("Entering main loop...");
	while (isRunning) {
		bool doRendering = false;

		long long startTime = Time::getTime();
		long long passedTime = startTime - lastTime;
		lastTime = startTime;

		unprocessedTime += passedTime / static_cast<double>(Time::Second);
		frameCounter += passedTime;

		while (unprocessedTime > frameTime) {
			doRendering = true;

			unprocessedTime -= frameTime;
		
			if (window->isCloseRequested()) {
				stop();
			}

			Time::setDelta(frameTime);

			Input::ifKeyPressed(GLFW_KEY_ESCAPE, [&]() {
				isRunning = false;
			});
			game->input();
			game->update();

			if (frameCounter >= Time::Second) {
				glfwSetWindowTitle(window->getWindowHandle(), string("FPS: " + to_string(frames)).c_str());
				frames = 0;
				frameCounter = 0;
			}
		}
		if (doRendering) {
			render();
			++frames;
		}
		else {
			glfwWaitEventsTimeout(0.001);
		}
	}
	cleanUp();
}

void Engine::render()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	ShaderUtil::bindShaders();
	game->render();
	window->render();

}

void Engine::cleanUp()
{
	ShaderUtil::disposeShaders();
	Text::dispose();
	window->dispose();
}
