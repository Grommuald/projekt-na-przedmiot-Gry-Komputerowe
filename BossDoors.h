#pragma once
#include "Doors.h"

class BossDoors :
	public Doors
{
public:
	BossDoors(const DungeonController::Location&);
	~BossDoors();
};

