#include "ContainerGUI.h"
#include "ResourceLoader.h"

ContainerGUI::ContainerGUI(const string& filename, const glm::vec2& position, const int& number_of_elements, 
	const int& uv_x, const int& uv_y, const int& uv_width, const int& uv_height, 
	const int& texture_width, const int& texture_height, const float& element_width, const float& element_height) :
	GUI(position, texture_width, texture_height, element_width, element_height, uv_x, uv_y, uv_width, uv_height)
{
	texture = ResourceLoader::loadTexture(filename);

	this->texture = texture;
	this->number_of_elements = number_of_elements;
}

ContainerGUI::~ContainerGUI()
{
}

void ContainerGUI::push(const size_t & val)
{
	number_of_elements += val;
}

void ContainerGUI::pop(const size_t & val)
{
	if (number_of_elements - val >= 0) {
		number_of_elements -= val;
	}
}

void ContainerGUI::setSize(const size_t & val)
{
	number_of_elements = val;
}

void ContainerGUI::draw()
{
	vertices.clear();
	uvs.clear();

	for (int i = 0; i < number_of_elements; ++i) {
		glm::vec2 vertex_up_left = glm::vec2(position.x + i*element_width, position.y + element_height);
		glm::vec2 vertex_up_right = glm::vec2(position.x + i*element_width + element_width, position.y + element_height);
		glm::vec2 vertex_down_right = glm::vec2(position.x + i*element_width + element_width, position.y);
		glm::vec2 vertex_down_left = glm::vec2(position.x + i*element_width, position.y);

		vertices.push_back(vertex_up_left);
		vertices.push_back(vertex_down_left);
		vertices.push_back(vertex_up_right);

		vertices.push_back(vertex_down_right);
		vertices.push_back(vertex_up_right);
		vertices.push_back(vertex_down_left);

		float uv_offset_x = uv_x / texture_width;
		float uv_offset_y = uv_y / texture_height;

		glm::vec2 uv_up_left = glm::vec2(uv_offset_x, uv_offset_y);
		glm::vec2 uv_up_right = glm::vec2(uv_offset_x + uv_width / texture_width, uv_offset_y);
		glm::vec2 uv_down_right = glm::vec2(uv_offset_x + uv_width / texture_width, uv_offset_y + uv_height / texture_height);
		glm::vec2 uv_down_left = glm::vec2(uv_offset_x, uv_offset_y + uv_height / texture_height);

		uvs.push_back(uv_up_left);
		uvs.push_back(uv_down_left);
		uvs.push_back(uv_up_right);

		uvs.push_back(uv_down_right);
		uvs.push_back(uv_up_right);
		uvs.push_back(uv_down_left);
	}
	if (number_of_elements > 0) {
		GUI::draw();
	}
}
