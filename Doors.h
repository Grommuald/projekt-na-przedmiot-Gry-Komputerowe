#pragma once
#include "Entity.h"
#include "DungeonController.h"
class Room;

class Doors :
	public Entity
{
public: 
	enum class DoorType { Plain, Boss, Key };

	Doors(const string& model_name, const string& texture_name, const DungeonController::Location&, const DoorType& type);
	~Doors();

	DungeonController::Location getLocation();
	DoorType getType() const { return type; }
	virtual void draw();
	virtual void moveDown();
	bool notLocked() const { return !locked; }
	void unlock() { locked = false; }
protected:
	bool locked = false;
	DoorType type;
	DungeonController::Location location;
	Sprite3D* gates;
};

