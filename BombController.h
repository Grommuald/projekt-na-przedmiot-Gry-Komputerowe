#pragma once
#include "Controller.h"

class Bomb;

class BombController :
	public Controller
{
public:
	BombController(Player*, Room*);
	~BombController();
	void spawnBomb(const glm::vec2& position, const Tile&);

	void update() final;
	void draw() final;
private:
	vector<Bomb*> bombs;
};

