#include "BossController.h"
#include "Boss.h"
#include "ShotController.h"
#include "Player.h"
#include "Time.h"
#include "Shot.h"
#include "Room.h"

BossController::BossController(Player* player, Boss* boss, Room* currentRoom) :
	Controller(player, currentRoom), boss(boss)
{
	shotTimer = new CountdownTimer();
	moveTimer = new CountdownTimer();
	canShoot = true;
	canMove = false;
	startedJump = false;
	endedJump = true;

	boss->setSpeed(1.5f);
	boss->setHealth(3.0f);
}


BossController::~BossController()
{
	delete shotTimer;
	delete moveTimer;
}

void BossController::update()
{
	if (boss->getHealth() <= 0.0f) {
		bossSlain = true;
	}
	else {
		shootingDirection = glm::vec2(player->getPosition().x - boss->getPosition().x,
			player->getPosition().y - boss->getPosition().y);
		shootingDirection = glm::normalize(shootingDirection);

		if (canShoot) {
			spawnShot(boss->getPosition(), shootingDirection, 1.0f, 1.0f);
			shotTimer->reset();
			canShoot = false;
		}
		if (!canShoot) {
			shotTimer->start(1000, [&]() {
				canShoot = true;
			});
		}

		if (canMove && endedJump) {
			jumpStartingPosition.x = boss->getPosition().x;
			jumpStartingPosition.y = boss->getPosition().y;
			if (boss->getPosition().x >= 1.48f &&
				player->getPosition().x >= boss->getPosition().x) {
				jumpDestination.x = boss->getPosition().x - 0.01f;
			}
			else if (boss->getPosition().x < -1.58f &&
				player->getPosition().x <= boss->getPosition().x) {
				jumpDestination.x = boss->getPosition().x + 0.01f;
			}
			else {
				jumpDestination.x = player->getPosition().x;
			}


			if (boss->getPosition().y >= 1.64f &&
				player->getPosition().y >= boss->getPosition().y) {
				jumpDestination.y = boss->getPosition().y - 0.01f;
			}
			else if (boss->getPosition().y <= 0.41f &&
				player->getPosition().y <= boss->getPosition().y) {
				jumpDestination.y = boss->getPosition().y + 0.01f;
			}
			else {
				jumpDestination.y = player->getPosition().y;
			}

			startedJump = true;
			canMove = false;

			jumpDirection = glm::vec2(jumpDestination.x - boss->getPosition().x, jumpDestination.y - boss->getPosition().y);
			jumpDirection = glm::normalize(jumpDirection);
		}
		if (!canMove && endedJump) {
			moveTimer->start(2000, [&]() {
				canMove = true;
			});
		}

		if (startedJump) {
			if (boss->getPosition().x + boss->getWidth() - 0.1f <= Room::ROOM_END_X && boss->getPosition().x - 0.2f >= Room::ROOM_START_X &&
				boss->getPosition().y + boss->getHeight() - 0.1f <= Room::ROOM_START_Y && boss->getPosition().y - 0.4f >= Room::ROOM_END_Y) {
				boss->setPosition(
					boss->getPosition().x + jumpDirection.x * boss->getSpeed() * Time::getDelta(),
					boss->getPosition().y + jumpDirection.y * boss->getSpeed() * Time::getDelta()
				);
			}
			else {
				if (boss->getPosition().x - 0.2f <= Room::ROOM_START_X) {
					boss->setPosition(boss->getPosition().x + 0.01f, boss->getPosition().y);
				}
				if (boss->getPosition().x + boss->getWidth() - 0.1f >= Room::ROOM_END_X) {
					boss->setPosition(boss->getPosition().x - 0.01f, boss->getPosition().y);
				}
				if (boss->getPosition().y - 0.4f <= Room::ROOM_END_Y) {
					boss->setPosition(boss->getPosition().x, boss->getPosition().y + 0.01f);
				}
				if (boss->getPosition().y + boss->getHeight() - 0.1f >= Room::ROOM_START_Y) {
					boss->setPosition(boss->getPosition().x, boss->getPosition().y - 0.01f);
				}
				endedJump = true;
				startedJump = false;

				moveTimer->reset();
				canMove = false;
			}
		}
		for (auto i : bullets) {
			i->update();
		}
	}
}

void BossController::draw()
{
	if (!bossSlain) {
		boss->draw();

		for (auto i : bullets) {
			i->draw();
		}
	}
}

void BossController::spawnShot(const glm::vec2 & position, const glm::vec2 & direction, const float & speed, const float & damage)
{
	bullets.push_back(new Shot(position, direction, speed, damage));
}