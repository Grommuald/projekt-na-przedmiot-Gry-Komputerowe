#pragma once
#include "Controller.h"

class Obstacle;
/*
class ObstacleController
{
public:
	static void spawnObstacle(const float&, const float&);
	static void spawnObstacle(const glm::vec2 & position);
	static void generateObstacles();
	static void updateObstacles();
	static void drawObstacles();
	static void disposeObstacles();
	static vector<TileStatus> getObstaclesGrid();
	static void setPlayer(Player*);
private:
	static vector<TileStatus> obstaclesGrid;
	static vector<Obstacle*> obstacles;
	static Player* player;
	static const unsigned short NUMBER_OF_PATTERNS;
};
*/

class ObstacleController : public Controller {
public:
	ObstacleController(Player*, Room*);
	~ObstacleController();

	//void spawnObstacle(const float&, const float&);
	//void spawnObstacle(const glm::vec2 & position);

	void update() final;
	void draw() final;
private:
};