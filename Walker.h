#pragma once
#include "Enemy.h"
class Walker :
	public Enemy
{
public:
	Walker(const int&, const int&);
	~Walker();

	void update() final;
	void setPath(vector<LocationDirection> path) final;
private:
	vector<LocationDirection> currentPath;
	LocationDirection pendingMove;

};

