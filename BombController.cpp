#include "BombController.h"
#include "Bomb.h"
#include "Room.h"
#include "Player.h"

BombController::BombController(Player* player, Room* currentRoom) : Controller(player, currentRoom)
{
}

BombController::~BombController()
{
}

void BombController::spawnBomb(const glm::vec2 & position, const Tile& tile_position)
{
	Bomb* newBomb = new Bomb(player->getPosition(), player->getCurrentTilePosition());
	bombs.push_back(newBomb);
}

void BombController::update()
{
	auto i = bombs.begin();
	while (i != bombs.end()) {
		(*i)->update();
		if ((*i)->ifExplodes()) {
			for (int j = 1; j <= (*i)->getTileRadius(); ++j) {
				if ((*i)->getCurrentTilePosition().x - j >= 0) {
					int index = (*i)->getCurrentTilePosition().x - j + Room::ROOM_WIDTH*(*i)->getCurrentTilePosition().y;
					if (currentRoom->getObstaclesGrid()->at(index) == TileStatus::Obstacle) {
						currentRoom->deleteObstacle((*i)->getCurrentTilePosition().x - j, (*i)->getCurrentTilePosition().y);
					}
				}
				if ((*i)->getCurrentTilePosition().x + j <= Room::ROOM_HEIGHT * (*i)->getCurrentTilePosition().y + Room::ROOM_WIDTH) {
					int index = (*i)->getCurrentTilePosition().x + j + Room::ROOM_WIDTH*(*i)->getCurrentTilePosition().y;
					if (currentRoom->getObstaclesGrid()->at(index) == TileStatus::Obstacle) {
						currentRoom->deleteObstacle((*i)->getCurrentTilePosition().x + j, (*i)->getCurrentTilePosition().y);
					}
				}
				if ((*i)->getCurrentTilePosition().y - j * Room::ROOM_WIDTH >= 0) {
					int index = (*i)->getCurrentTilePosition().x + (*i)->getCurrentTilePosition().y - j * Room::ROOM_WIDTH;
					if (currentRoom->getObstaclesGrid()->at(index) == TileStatus::Obstacle) {
						currentRoom->deleteObstacle((*i)->getCurrentTilePosition().x, (*i)->getCurrentTilePosition().y - j);
					}
				}
				if (((*i)->getCurrentTilePosition().y + j) * Room::ROOM_WIDTH < Room::ROOM_WIDTH * Room::ROOM_HEIGHT) {
					int index = (*i)->getCurrentTilePosition().x + ((*i)->getCurrentTilePosition().y + j) * Room::ROOM_WIDTH;
					if (currentRoom->getObstaclesGrid()->at(index) == TileStatus::Obstacle) {
						currentRoom->deleteObstacle((*i)->getCurrentTilePosition().x, (*i)->getCurrentTilePosition().y + j);
					}
				}
			}
			delete (*i);
			i = bombs.erase(i);
		}
		if (i != bombs.end()) {
			++i;
		}
	}
}

void BombController::draw()
{
	for (auto i : bombs) {
		i->draw();
	}
}
