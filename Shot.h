#pragma once
#include "Entity.h"

class Shot : public Entity
{
public:
	Shot(const glm::vec2&, const glm::vec2&, const float&, const float&);
	~Shot();

	void updateTilePosition();
	void update();
	void draw();
private:
	float speed;
	float damage;

	float size;
};

