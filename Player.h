#pragma once
#include "Headers.h"
#include "Entity.h"

class Sprite3D;
class CountdownTimer;
class ShotSpawner;
class ShotController;
class BombController;
class HUD;

class Player : public Entity
{
public:
	Player();
	Player(const int&, const int&);
	~Player();

	void input();
	void update();
	void draw();
	void updateTilePosition();

	void setShotController(ShotController*);
	void setBombController(BombController*);

	float getDamage() const { return damage; }
	void setDamage(const float& val) { damage = val; }

	float getSpeed() const { return speed; }
	void setSpeed(const float& val) { speed = val; }

	float getShootingRange() const { return shootingRange; }
	void setShootingRange(const float& val) { shootingRange = val; }

	float getShootingFrequency() const { return shootingFrequency; }
	void setShootingFrequency(const float& val) { shootingFrequency = val; }

	float getSotSpeed() const { return shotSpeed; }
	void setShotSpeed(const float& val) { shotSpeed = val; }

	void setCollidesWithDoors(const bool& val) { collidesWithDoors = val; }
	bool ifCollidesWithDoors() const { return collidesWithDoors; }

	int getDungeonX() const { return dungeon_x; }
	void setDungeonX(const int& val) { dungeon_x = val; }
	int getDungeonY() const { return dungeon_y; }
	void setDungeonY(const int& val) { dungeon_y = val; }
	void setDungeonPosition(const int& x, const int& y);

	bool hasObtainedKey() const { return keyObtained; }
	void setKeyObtained(const bool& val) { keyObtained = val; }

	void setHealth(const int& val) { healthContainers = val; }
	int getHealth() const { return healthContainers; }

	bool isPlayerDead() const { return isDead; }

	HUD* getHUD() const { return hud; }
	void setHUD(HUD* hud) { this->hud = hud; }
	void updateHUDHealth();
	void updateHUDKey();

	void setCanShoot(const bool& val);
private:
	bool isDead = false;

	float shootingRange;
	float shootingFrequency;
	float damage;
	float speed;
	float shotSpeed;

	int healthContainers;

	int dungeon_x;
	int dungeon_y;

	bool canShoot;
	bool canPlaceBomb;
	bool collidesWithDoors;

	bool keyObtained = false;

	HUD* hud;

	ShotSpawner* shotSpawner;
	CountdownTimer* shootingTimer;
	CountdownTimer* bombPlacementTimer;
	ShotController* shotController;
	BombController* bombController;
};

