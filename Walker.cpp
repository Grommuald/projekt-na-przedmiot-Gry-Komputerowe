#include "Walker.h"
#include "Time.h"


Walker::Walker(const int& x, const int& y) : Enemy("enemy.obj", "enemy_texture.dds", x, y)
{
	id = "walker";
	pendingMove = LocationDirection::None;
}


Walker::~Walker()
{
}

void Walker::update()
{
	Enemy::update();
	updateTilePosition();
	if (previousTilePosition.x == currentTilePosition.x && previousTilePosition.y == currentTilePosition.y) {
		switch (pendingMove) {
		case LocationDirection::North:
			setDirection(0, 1);
			setPosition(getPosition() + getDirection() * (float)Time::getDelta() * speed);
			break;
		case LocationDirection::South:
			setDirection(0, -1);
			setPosition(getPosition() + getDirection() * (float)Time::getDelta() * speed);
			break;
		case LocationDirection::West:
			setDirection(-1, 0);
			setPosition(getPosition() + getDirection() * (float)Time::getDelta() * speed);
			break;
		case LocationDirection::East:
			setDirection(1, 0);
			setPosition(getPosition() + getDirection() * (float)Time::getDelta() * speed);
			break;
		}
	}
	else {
		if (!currentPath.empty()) {
			currentPath.erase(currentPath.begin());
			if (!currentPath.empty()) {
				pendingMove = currentPath[0];
				previousTilePosition = currentTilePosition;
			}
		}
		else {
			pendingMove = LocationDirection::None;
		}
	}
}

void Walker::setPath(vector<LocationDirection> path)
{
	currentPath = path;
	if (!currentPath.empty()) {
		pendingMove = currentPath[0];
	}
	previousTilePosition = currentTilePosition;
}
