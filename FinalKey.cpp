#include "FinalKey.h"
#include "Sprite3D.h"
#include "Time.h"

FinalKey::FinalKey(const int& tile_x, const int& tile_y) : Entity("key.obj", "key_texture.dds", "key", tile_x, tile_y)
{
	sprite->setScale(0.1f);
	setWidth(0.1f);
	setHeight(0.1f);
}


FinalKey::~FinalKey()
{
}

void FinalKey::draw()
{
	static float rotation = 0.0f;
	sprite->setRotation(glm::vec3(1, 0, 0), rotation);
	rotation += 1.0f;

	sprite->draw();
}
