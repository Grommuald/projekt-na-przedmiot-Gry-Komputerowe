#pragma once
#include "Enemy.h"
class Boss :
	public Enemy
{
public:
	Boss(const int& x, const int& y);
	~Boss();

	void setSpeed(const float& speed) { this->speed = speed; }
	float getSpeed() const { return speed; }
private:
	float speed = 1.0f;
};

