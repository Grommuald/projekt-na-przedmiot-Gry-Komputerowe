#pragma once
#include "Headers.h"

class Window;
class Game;

class Engine
{
public:
	const int Width = 1270;
	const int Height = 720;
	const std::string Title = "Main Window";
	const double FrameCap = 5000.0;

	Engine();
	~Engine();

	void start();
	void stop();
private:
	Window* window;
	Game* game;
	bool isRunning;

	void initializeGraphics();
	void run();
	void render();
	void cleanUp();
};
