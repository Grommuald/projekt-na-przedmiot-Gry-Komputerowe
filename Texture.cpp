#include "Texture.h"

Texture::Texture(const GLuint& id) : id(id)
{
}


Texture::~Texture()
{
}

GLuint Texture::getId() const
{
	return id;
}

void Texture::bind()
{
	glBindTexture(GL_TEXTURE_2D, id);
}
