#include "Room.h"
#include "Entity.h"
#include "Obstacle.h"
#include "FinalKey.h"

const short Room::ROOM_WIDTH = 19;
const short Room::ROOM_HEIGHT = 9;
const float Room::ROOM_START_X = -1.8f;
const float Room::ROOM_START_Y = 1.85f;
const float Room::ROOM_END_X = 1.8f;
const float Room::ROOM_END_Y = 0.0f;

Room::Room() : Room(0, 0)
{
	
}

Room::Room(const size_t & dungeon_x, const size_t & dungeon_y) : dungeon_x(dungeon_x), dungeon_y(dungeon_y)
{
	enemies = new vector<Enemy*>();
	obstacles = new vector<Obstacle*>();
	doors = new vector<Doors*>();
}

Room::~Room()
{
	delete enemies;
	delete obstacles;
}

void Room::addToObstaclesGrid(const TileStatus &val)
{
	obstaclesGrid.push_back(val);
}

void Room::addToObstaclesCoords(const Tile &tile)
{
	obstaclesCoords.push_back(tile);
}

void Room::addObstacle(Obstacle *o)
{
	obstacles->push_back(o);
}

void Room::addEnemy(Enemy *e)
{
	enemies->push_back(e);
}

void Room::addKey(FinalKey *k)
{
	key = k;
}

void Room::addDoors(Doors *doors)
{
	this->doors->push_back(doors);
}

void Room::deleteObstacle(const int& tile_x, const int& tile_y)
{
	for (auto i = obstaclesCoords.begin(); i != obstaclesCoords.end(); ++i) {
		if ((*i).x == tile_x && (*i).y == tile_y) {
			cout << "found" << endl;
			obstaclesCoords.erase(i);
			break;
		}
	}
	obstaclesGrid[tile_x + tile_y * Room::ROOM_WIDTH] = TileStatus::Empty;

	for (auto i = obstacles->begin(); i != obstacles->end(); ++i) {
		if ((*i)->getCurrentTilePosition().x == tile_x && 
			(*i)->getCurrentTilePosition().y == tile_y) {
			delete (*i);
			obstacles->erase(i);
			break;
		}
	}
}

void Room::destroyKey()
{
	if (key) {
		delete key;
		key = nullptr;
	}
}
