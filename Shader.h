#pragma once
#include "ResourceLoader.h"

class Shader
{
public:
	Shader();
	virtual ~Shader();

	void createProgram();

	void addVertexShader(const std::string&);
	void addFragmentShader(const std::string&);
	void addGeometryShader(const std::string&);
	
	void addUniform(const std::string&);

	void setUniform(const std::string&, const int&);
	void setUniform(const std::string&, const float&);
	void setUniform(const std::string&, const glm::vec3&);
	void setUniform(const std::string&, const glm::mat4&);

	void compileShader();
	void bind();
private:
	int program;

	void addProgram(const std::string&, const int&);
	void showErrorInfo(const int&);

	std::unordered_map<std::string, int>* uniforms;

	GLuint shader;
};

