#pragma once
#include "Headers.h"

class Camera;

class Transform
{
public:
	static glm::mat4 getTransformation();
	static glm::vec3 getTranslation();
	static glm::vec4 getRotation();
	static glm::vec3 getScale();

	static void setTranslation(const glm::vec3&);
	static void setTranslation(const float&, const float&, const float&);
	static void setRotation(const glm::vec4&);
	static void setRotation(const float&, const float&, const float&, const float&);
	static void setScale(const glm::vec3&);
	static void setScale(const float&, const float&, const float&);

	static glm::mat4 getProjectedTransformation();
	static void setProjection(const float&, const float&, const float&, const float&, const float&);
private:
	static glm::vec3 translation;
	static glm::vec4 rotation;
	static glm::vec3 scale;

	static float zNear;
	static float zFar;
	static float width, height;
	static float fov;
};

