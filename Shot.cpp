#include "Shot.h"
#include "Sprite3D.h"
#include "Time.h"
#include "Room.h"

Shot::Shot(const glm::vec2 &position, const glm::vec2 &direction, const float &speed, const float& damage) : Entity("orb.obj", "stone.bmp", "players_shot", position, direction), speed(speed), damage(damage)
{
	setDirection(direction);
	sprite->setScale(0.1f);
	sprite->setRenderingHeight(0.0f);

	setWidth(0.1f);
	setHeight(0.1f);
}

Shot::~Shot()
{

}

void Shot::updateTilePosition()
{
	currentTilePosition.x = static_cast<int>((getPosition().x + 0.1f + 1.80f) / 3.60f * 1000.0f) / 55;
	currentTilePosition.y = Room::ROOM_HEIGHT - static_cast<int>(((getPosition().y - 0.1f) / 1.60f)* 1000.0f) / 125 - 1;
}

void Shot::update()
{
	setPosition(getPosition() + (getDirection() * speed * (float)Time::getDelta()));
	updateTilePosition();
}

void Shot::draw()
{
	Entity::draw();
}
