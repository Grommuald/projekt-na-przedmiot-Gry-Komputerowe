#include "ShaderUtil.h"


Shader* ShaderUtil::vertexShader = new Shader();
Shader* ShaderUtil::textShader = new Shader();
Shader* ShaderUtil::fadeShader = new Shader();

void ShaderUtil::initializeShaders()
{
	vertexShader->createProgram();
	vertexShader->addVertexShader(ResourceLoader::loadShader("basicVertex.vs"));
	vertexShader->addFragmentShader(ResourceLoader::loadShader("basicTexture.fs"));

	vertexShader->compileShader();

	vertexShader->addUniform("MVP");
	vertexShader->addUniform("V");
	vertexShader->addUniform("M");
	vertexShader->addUniform("light_position_worldspace");
	vertexShader->addUniform("textureSampler");

	textShader->createProgram();
	textShader->addVertexShader(ResourceLoader::loadShader("textVertex.vs"));
	textShader->addFragmentShader(ResourceLoader::loadShader("textFragment.fs"));

	textShader->compileShader();

	textShader->addUniform("textureSampler");

	fadeShader->createProgram();
	fadeShader->addVertexShader(ResourceLoader::loadShader("textVertex.vs"));
	fadeShader->addFragmentShader(ResourceLoader::loadShader("fadeFragment.fs"));

	fadeShader->compileShader();

	fadeShader->addUniform("textureSampler");
	fadeShader->addUniform("alpha");
}

void ShaderUtil::bindShaders()
{
	vertexShader->bind();
}

void ShaderUtil::disposeShaders()
{
	delete vertexShader;
	delete textShader;
	delete fadeShader;
}

Shader * ShaderUtil::getVertexShader()
{
	return vertexShader;
}

Shader * ShaderUtil::getTextShader()
{
	return textShader;
}

Shader * ShaderUtil::getFadeShader()
{
	return fadeShader;
}
