#pragma once
#include "Headers.h"
class Window;

class Camera
{
public:
	static void initialize(Window *window, glm::vec3);
	static void calculateTransformation();
	static void input();
	
	static glm::mat4 getMVP();
	static glm::mat4 getModel();
	static glm::mat4 getProjection();
	static glm::mat4 getView();

	static glm::vec3 getPosition();
	static glm::vec3 getUp();
	static glm::vec3 getRight();
	static glm::vec3 getDirection();
private:
	static glm::mat4 modelMatrix;
	static glm::mat4 viewMatrix;
	static glm::mat4 projectionMatrix;
	static glm::mat4 mvp;

	static glm::vec3 position;
	static glm::vec3 direction;
	static glm::vec3 up;
	static glm::vec3 right;

	static float horizontalAngle;
	static float verticalAngle;

	static float initialFoV;

	static float speed;
	static float mouseSpeed;

	static Window* windowHandle;
};

