#pragma once
#include "Headers.h"

struct SFace {
	int v[3];
	int n[3];
	int t[3];
};

class Util
{
public:
	static vector<string>& Util::splitByCharacter(const string &str, const char&c);
	static string trim(const string&);
	static vector<float> getNumbers(const string& str);
	static bool containsPhrase(const string&, const string&);
	static float lerp(const float&, const float&, const float&);
	static bool isNumeric(const string& input);
};

