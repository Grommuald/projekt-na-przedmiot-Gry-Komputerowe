#include "Util.h"

vector<string>& Util::splitByCharacter(const string &str, const char&c)
{
	vector<string>* result = new vector<string>();
	if (str.length() <= 1) {
		return *result;
	}

	string restOfString = str;

	while (!restOfString.empty()) {
		for (int i = 0; i < restOfString.length(); ++i) {
			if (restOfString[i] == c) {
				if (i > 0) {
					result->push_back(restOfString.substr(0, i));
					restOfString = restOfString.substr(i + 1, restOfString.length() - i - 1);
				}
				else {
					if (restOfString.length() == 1)
						restOfString.clear();
					else {
						restOfString = restOfString.substr(i + 1, restOfString.length() - i - 1);
					}
				}
				break;
			}
			else {
				if (i == restOfString.length() - 1) {
					result->push_back(restOfString.substr(0, restOfString.length()));
					restOfString.clear();
					break;
				}
			}
		}
	}
	return *result;
}
string Util::trim(const string& str) {
	std::string tmp = str;

	for (auto i = tmp.begin(); i != tmp.end(); ++i) {
		if (*i == '\t') {
			*i = ' ';
		}
	}
	tmp.erase(std::unique(tmp.begin(), tmp.end(),
		[](char a, char b) { return a == ' ' && b == ' '; }), tmp.end());
	return tmp;
}

vector<float> Util::getNumbers(const string & str)
{
	auto isNumber = [](const char& c) {
		return ((c >= '0' && c <= '9') || c == '.' || c == '-');
	};
	int numbersCount = 0;
	for (int i = 0; i < str.length(); ++i) {
		if (isNumber(str[i]) && !isNumber(str[i + 1]))
			numbersCount++;
	}
	int* starts = new int[numbersCount];
	int* ends = new int[numbersCount];

	int startsIndex = 0, endsIndex = 0;

	for (int i = 0; i < str.length(); ++i) {
		if (!isNumber(str[i]) && isNumber(str[i + 1]))
		{
			starts[startsIndex] = i + 1;
			startsIndex++;
		}
		if (isNumber(str[i]) && !isNumber(str[i + 1]))
		{
			ends[endsIndex] = i;
			endsIndex++;
		}
	}

	char numberAsText[30];
	vector<float> numbers;
	for (int i = 0; i < numbersCount; i++)
	{
		int letterIndex = 0;

		for (int j = starts[i]; j <= ends[i]; j++)
		{
			numberAsText[letterIndex] = str[j];
			letterIndex++;
		}
		numberAsText[letterIndex] = '\0';

		numbers.push_back(static_cast<float>(atof(numberAsText)));
	}
	return numbers;
}

bool Util::containsPhrase(const string &str, const string& phrase)
{
	if (str.find(phrase) != string::npos)
		return true;
	return false;
}

float Util::lerp(const float &a, const float &b, const float &f)
{
	return a + f * (b - a);
}

bool Util::isNumeric(const string & input)
{
	return std::all_of(input.begin(), input.end(), ::isdigit);
}
