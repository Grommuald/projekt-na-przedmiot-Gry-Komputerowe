#include "Controller.h"

Controller::Controller(Player* player, Room* room) : player(player), currentRoom(room)
{
}

Controller::Controller(Player *player) : player(player), currentRoom(nullptr)
{
}


Controller::~Controller()
{
}

void Controller::setCurrentRoom(Room *room)
{
	currentRoom = room;
}
