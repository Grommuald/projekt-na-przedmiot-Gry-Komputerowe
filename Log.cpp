#include "Log.h"

void Log::logFatalError(const std::string &message)
{
	std::string errorMessage = "Error: " + message;
	puts(errorMessage.c_str());
	system("pause");
	exit(EXIT_FAILURE);
}

void Log::logError(const std::string &message) {
	std::string errorMessage = "Error: " + message;
	puts(errorMessage.c_str());
}
void Log::log(const std::string &message)
{
	std::string messageInfo = "Info: " + message;
	puts(messageInfo.c_str());
}

void Log::rawLog(const std::string &message)
{
	puts(message.c_str());
}

void Log::log(const std::stringstream &s)
{
	std::string messageInfo = "Info: " + s.str();
	puts(s.str().c_str());
}
