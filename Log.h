#pragma once
#include "Headers.h"

class Log
{
public:
	static void logFatalError(const std::string&);
	static void logError(const std::string&);
	static void log(const std::string&);
	static void rawLog(const std::string&);
	static void log(const std::stringstream&);

};

