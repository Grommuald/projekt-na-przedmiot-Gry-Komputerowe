#pragma once
#include "Entity.h"
class Player;
class Room;

class Controller
{
public:
	Controller(Player*, Room*);
	Controller(Player*);
	virtual ~Controller();

	virtual void draw() = 0;
	virtual void update() = 0;

	virtual void setCurrentRoom(Room*);
	virtual void restart() {}
protected:
	Room* currentRoom;
	Player* player;
};

