#pragma once
#include "Headers.h"

class Mesh;
class Texture;

class ResourceLoader
{
public:
	static Mesh* loadMesh(const string& filename);
	static Texture* loadTexture(const string& filename);
	static string loadShader(const string& filename);
	static short* loadRoomPattern(const string& filename);
private:
	static Texture* loadBMP(const string& filename);
	static Texture* loadDDS(const string& filename);
};

