#include "PlainDoors.h"



PlainDoors::PlainDoors(const DungeonController::Location& location) :
	Doors("doors.obj", "doors_texture.dds", location, DoorType::Plain)
{
}


PlainDoors::~PlainDoors()
{
}
