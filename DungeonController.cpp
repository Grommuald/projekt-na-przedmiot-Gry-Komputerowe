#include "DungeonController.h"
#include "RoomController.h"

const size_t DungeonController::GRID_SIZE = 9;

DungeonController::DungeonController(const size_t& numberOfRooms, Player* player) 
	: Controller(player), numberOfRooms(numberOfRooms)
{
	//roomController = new RoomController(player);

	dungeon = vector<RoomType>(GRID_SIZE * GRID_SIZE);
	for (auto& i : dungeon) {
		i = RoomType::EMPTY_SPACE;
	}
	generateDungeon();
}


DungeonController::~DungeonController()
{
	delete roomController;
}
DungeonController::RoomType DungeonController::getRoomType(const int& x, const int& y) const {
	return dungeon[x + y*GRID_SIZE];
}
void DungeonController::generateDungeon()
{
	glm::vec2 startingPos(GRID_SIZE / 2, GRID_SIZE / 2);
	dungeon[startingPos.x + startingPos.y*GRID_SIZE] = RoomType::STARTING_ROOM;

	vector<glm::vec2> rooms;
	rooms.push_back(startingPos);

	auto addRoom = [&](glm::vec2 _c, vector<glm::vec2>& _possibilities) {
		bool foundDuplicate = false;
		for (glm::vec2 p : _possibilities) {
			if (p.x == _c.x && p.y == _c.y) {
				foundDuplicate = true;
				break;
			}
		}
		if (!foundDuplicate) {
			_possibilities.push_back(_c);
		}
	};
	srand(static_cast<unsigned>(time(0)));
	// numberOfRooms - 2, -1 for starting room, -1 for boss room
	for (int i = 0; i < numberOfRooms - 2; ++i) {
		vector<glm::vec2> possibilities;
		for (glm::vec2 c : rooms) {
			if (c.y + 1 < GRID_SIZE && dungeon[c.x + (c.y + 1)*GRID_SIZE] == RoomType::EMPTY_SPACE) {
				glm::vec2 newPos(c.x, c.y + 1);
				addRoom(newPos, possibilities);
			}
			if (c.y - 1 >= 0 && dungeon[c.x + (c.y - 1)*GRID_SIZE] == RoomType::EMPTY_SPACE) {
				glm::vec2 newPos(c.x, c.y - 1);
				addRoom(newPos, possibilities);
			}
			if (c.x + 1 < GRID_SIZE && dungeon[c.x + 1 + c.y*GRID_SIZE] == RoomType::EMPTY_SPACE) {
				glm::vec2 newPos(c.x + 1, c.y);
				addRoom(newPos, possibilities);
			}
			if (c.x - 1 >= 0 && dungeon[c.x - 1 + c.y*GRID_SIZE] == RoomType::EMPTY_SPACE) {
				glm::vec2 newPos(c.x - 1, c.y);
				addRoom(newPos, possibilities);
			}
		}
		if (possibilities.size() > 0) {
			bool roomAdded = false;

			if (possibilities.size() == 1) {
				rooms.push_back(possibilities[0]);
				++numberOfAddedRooms;
			}
			else {
				while (possibilities.size() > 1 && !roomAdded) {
					int randomizedIndex = rand() % possibilities.size();
					glm::vec2 _c = possibilities[randomizedIndex];
					unsigned char numberOfNeighbours = 0;

					if (_c.y + 1 < GRID_SIZE && dungeon[_c.x + (_c.y + 1)*GRID_SIZE] != RoomType::EMPTY_SPACE) {
						++numberOfNeighbours;
					}
					if (_c.y - 1 >= 0 && dungeon[_c.x + (_c.y - 1)*GRID_SIZE] != RoomType::EMPTY_SPACE) {
						++numberOfNeighbours;
					}
					if (_c.x + 1 < GRID_SIZE && dungeon[_c.x + 1 + _c.y*GRID_SIZE] != RoomType::EMPTY_SPACE) {
						++numberOfNeighbours;
					}
					if (_c.x - 1 >= 0 && dungeon[_c.x - 1 + _c.y*GRID_SIZE] != RoomType::EMPTY_SPACE) {
						++numberOfNeighbours;
					}

					if (numberOfNeighbours <= 1) {
						rooms.push_back(_c);
						dungeon[_c.x + GRID_SIZE *_c.y] = RoomType::PLAIN_ROOM;

						roomAdded = true;
						++numberOfAddedRooms;
					}
					else {
						swap(possibilities[randomizedIndex], possibilities[possibilities.size() - 1]);
						possibilities.pop_back();
					}
				}
			}
		}
	}
	// finding room that has only one neighboor (for boss room)
	vector<glm::vec2> standAloneRooms; // rooms with only one neighbour
	for (glm::vec2 _c : rooms) {
		unsigned char numberOfNeighbours = 0;
		if (_c.y + 1 < GRID_SIZE && dungeon[_c.x + (_c.y + 1)*GRID_SIZE] != RoomType::EMPTY_SPACE) {
			++numberOfNeighbours;
		}
		if (_c.y - 1 >= 0 && dungeon[_c.x + (_c.y - 1)*GRID_SIZE] != RoomType::EMPTY_SPACE) {
			++numberOfNeighbours;
		}
		if (_c.x + 1 < GRID_SIZE && dungeon[_c.x + 1 + _c.y*GRID_SIZE] != RoomType::EMPTY_SPACE) {
			++numberOfNeighbours;
		}
		if (_c.x - 1 >= 0 && dungeon[_c.x - 1 + _c.y*GRID_SIZE] != RoomType::EMPTY_SPACE) {
			++numberOfNeighbours;
		}
		if (numberOfNeighbours <= 1 && dungeon[_c.x + GRID_SIZE * _c.y] != RoomType::STARTING_ROOM) {
			standAloneRooms.push_back(_c);
		}
	}
	int greatestDistanceToStartingRoom = 0;
	glm::vec2 furthestRoom = rooms[0];
	vector<glm::vec2>::iterator furthestRoomIterator;

	for (auto i = standAloneRooms.begin() + 1; i != standAloneRooms.end(); ++i) {
		int distanceOfCurrentRoom = static_cast<int>(sqrt(pow((*i).x - rooms[0].x, 2) + pow((*i).y - rooms[0].y, 2)));
		if (distanceOfCurrentRoom > greatestDistanceToStartingRoom) {
			greatestDistanceToStartingRoom = distanceOfCurrentRoom;
			furthestRoom = *i;
			furthestRoomIterator = i;
		}
	}
	dungeon[furthestRoom.x + furthestRoom.y*GRID_SIZE] = RoomType::BOSS_ROOM;
	bossRoomPosition = glm::vec2(furthestRoom.x, furthestRoom.y);

	swap(*furthestRoomIterator, standAloneRooms[standAloneRooms.size() - 1]);
	standAloneRooms.pop_back();
	// treasure room
	glm::vec2 treasureRoom = standAloneRooms[rand() % standAloneRooms.size()];
	dungeon[treasureRoom.x + GRID_SIZE*treasureRoom.y] = RoomType::TREASURE_ROOM;
}

void DungeonController::draw()
{
	//roomController->drawCurrentRoom();
}

void DungeonController::update()
{
	//roomController->updateCurrentRoom();
}

void DungeonController::print()
{
	for (int i = 0; i < GRID_SIZE; ++i) {
		for (int j = 0; j < GRID_SIZE; ++j) {
			if (dungeon[j + GRID_SIZE*i] == RoomType::EMPTY_SPACE)
				cout << " ";
			else if (dungeon[j + GRID_SIZE*i] == RoomType::PLAIN_ROOM)
				cout << 'r';
			else if (dungeon[j + GRID_SIZE*i] == RoomType::STARTING_ROOM)
				cout << 's';
			else if (dungeon[j + GRID_SIZE*i] == RoomType::BOSS_ROOM)
				cout << 'b';
			else if (dungeon[j + GRID_SIZE*i] == RoomType::TREASURE_ROOM)
				cout << 't';
			cout << " ";
		}
		cout << endl;
	}
}

vector<DungeonController::DoorLocation> DungeonController::getRoomsNeighbours(const size_t & x, const size_t & y)
{
	int room_location = x + GRID_SIZE*y;
	if (room_location < 0 || room_location >= GRID_SIZE * GRID_SIZE || dungeon[room_location] == RoomType::EMPTY_SPACE) {
		return vector<DoorLocation>(0);
	}
	vector<DoorLocation> locations;
	DoorLocation doorLocation = { Location::None, RoomType::EMPTY_SPACE };
	
	if (room_location - 1 >= GRID_SIZE * y && dungeon[room_location - 1] != RoomType::EMPTY_SPACE) {
		doorLocation.location = Location::Left;
		if (dungeon[room_location - 1] == RoomType::BOSS_ROOM) {
			doorLocation.roomType = RoomType::BOSS_ROOM;
		}
		else {
			doorLocation.roomType = RoomType::PLAIN_ROOM;
		}
		locations.push_back(doorLocation);
	}
	if (room_location + 1 < GRID_SIZE * y + GRID_SIZE && dungeon[room_location + 1] != RoomType::EMPTY_SPACE) {
		doorLocation.location = Location::Right;
		if (dungeon[room_location + 1] == RoomType::BOSS_ROOM) {
			doorLocation.roomType = RoomType::BOSS_ROOM;
		}
		else {
			doorLocation.roomType = RoomType::PLAIN_ROOM;
		}
		locations.push_back(doorLocation);
	}
	if (y > 0 && room_location - GRID_SIZE >= 0 && dungeon[room_location - GRID_SIZE] != RoomType::EMPTY_SPACE) {
		doorLocation.location = Location::Top;
		if (dungeon[room_location - GRID_SIZE] == RoomType::BOSS_ROOM) {
			doorLocation.roomType = RoomType::BOSS_ROOM;
		}
		else {
			doorLocation.roomType = RoomType::PLAIN_ROOM;
		}
		locations.push_back(doorLocation);
	}
	if (y < GRID_SIZE - 1 && room_location + GRID_SIZE < GRID_SIZE*GRID_SIZE && dungeon[room_location + GRID_SIZE] != RoomType::EMPTY_SPACE) {
		doorLocation.location = Location::Bottom;
		if (dungeon[room_location + GRID_SIZE] == RoomType::BOSS_ROOM) {
			doorLocation.roomType = RoomType::BOSS_ROOM;
		}
		else {
			doorLocation.roomType = RoomType::PLAIN_ROOM;
		}
		locations.push_back(doorLocation);
	}
	return locations;
}
