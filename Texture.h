#pragma once
#include "Headers.h"

class Texture
{
public:
	Texture(const GLuint&);
	~Texture();

	GLuint getId() const;
	void bind();

private:
	GLuint id;
};

