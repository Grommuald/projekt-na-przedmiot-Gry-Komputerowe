#include "Game.h"
#include "Input.h"
#include "Window.h"
#include "Time.h"
#include "Log.h"
#include "Mesh.h"
#include "ResourceLoader.h"
#include "Sprite3D.h"
#include "Camera.h"
#include "Shader.h"
#include "Transform.h"
#include "ShaderUtil.h"
#include "Text.h"
#include "Engine.h"
#include "ContainerGUI.h"
#include "IconValueGUI.h"
#include "Player.h"
#include "ShotController.h"
#include "ObstacleController.h"
#include "AIController.h"
#include "RoomController.h"
#include "Doors.h"
#include "HUD.h"
#include "FadingCaption.h"

Game::Game(Window* handle) : windowHandle(handle)
{
	room = new Sprite3D("room2.obj", "room_texture.dds");

	player = new Player(0, 0);
	roomController = new RoomController(player);
	hud = new HUD(player, 1280, 720, player->getHealth());
	player->setHUD(hud);

	room->setScale(0.1f);

	Camera::initialize(windowHandle, glm::vec3(-0.005f, 1.02f, 2.75f));
	Transform::setProjection(70.0f, windowHandle->getWindowWidth(), windowHandle->getWindowHeight(), 0.1f, 1000.0f);
}

Game::~Game()
{
	delete room;
}

void Game::input()
{
	if (!roomController->getEndCondition()) {
		player->input();
	}
	else {
		Input::ifKeyPressed(GLFW_KEY_SPACE, [&]() {
			roomController->restart();
		});
	}
}

void Game::update()
{
	roomController->update();
}

void Game::render()
{
	Camera::calculateTransformation();
	ShaderUtil::getVertexShader()->setUniform("light_position_worldspace", glm::vec3(0, 1, 1.5f));

	room->draw();
	player->draw();

	roomController->draw();
	if (!roomController->getEndCondition()) {
		hud->draw();
	}
}