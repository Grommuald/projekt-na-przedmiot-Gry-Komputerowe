#include "Window.h"
#include "Input.h"
#include "Log.h"
#include "Callbacks.h"
#include "Util.h"

Window::Window()
{
}

Window::~Window()
{
}

void Window::createWindow(const int & width, const int & height, const std::string & title)
{
	windowTitle = title;
	windowWidth = width;
	windowHeight = height;

	// setting title
	Log::log("Initializing GLFW...");
	if (!glfwInit()) {
		puts("Failed to init GLFW.");
		exit(EXIT_FAILURE);
	}
	// setting display mode (width, height)
	Log::log("Getting window handle...");
	windowHandle = glfwCreateWindow(width, height, title.c_str(), nullptr, nullptr);
	if (!windowHandle) {
		glfwTerminate();
		Log::logFatalError("Failed to create window handle.");
	}
	glfwMakeContextCurrent(windowHandle);
	glfwSetWindowSizeCallback(windowHandle, Callbacks::window_size_callback);
	glfwSetKeyCallback(windowHandle, Input::key_callback);

	glfwSetInputMode(windowHandle, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

	glfwPollEvents();
	glfwSetCursorPos(windowHandle, windowWidth / 2, windowHeight / 2);

	Log::log("Initializing GLEW...");
	if (glewInit() != GLEW_OK) {
		glfwTerminate();
		Log::logFatalError("Failed to initialize GLEW...");
	}
	glfwSwapInterval(1);
}

void Window::render()
{
	glfwSwapBuffers(windowHandle);
	glfwPollEvents();
}

void Window::dispose()
{
	glfwDestroyWindow(windowHandle);

	glfwTerminate();
	exit(EXIT_SUCCESS);
}

bool Window::isCloseRequested()
{
	return glfwWindowShouldClose(windowHandle);
}

int Window::getWindowWidth()
{
	return getWindowSize().x;
}

int Window::getWindowHeight()
{
	return getWindowSize().y;
}

void Window::setWindowWidth(const int & width)
{
	windowWidth = width;
}
void Window::setWindowHeight(const int & height)
{
	windowHeight = height;
}

double Window::getMouseX() const
{
	return getMouseCoords().x;
}

double Window::getMouseY() const
{
	return getMouseCoords().y;
}

void Window::setMouse(const double &x, const double &y)
{
	glfwSetCursorPos(windowHandle, x, y);
}

std::string Window::getTitle()
{
	return windowTitle;
}

GLFWwindow * Window::getWindowHandle()
{
	return windowHandle;
}

glm::vec2 Window::getWindowSize() const
{
	int width, height;
	glfwGetWindowSize(windowHandle, &width, &height);
	return glm::vec2(width, height);
}

glm::vec2 Window::getMouseCoords() const
{
	double x, y;
	glfwGetCursorPos(windowHandle, &x, &y);
	return glm::vec2(x, y);
}
