#pragma once
#include "Headers.h"

class Window;

enum KeyState {
	PRESSED, DOWN, UP, NONE
};

struct Key {
public:
	int keyId;
	KeyState state;
};

class Input
{
public:
	static void ifKeyPressed(const int&, std::function<void()>);
	static void ifKeyDown(const int&, std::function<void()>);
	static void ifKeyUp(const int&, std::function<void()>);
	
	static void initialize();

	static void key_callback(GLFWwindow*, int, int, int, int);
private:
	static void resetKey(const int&);

	static const int NUM_KEYCODES;
	static Key* keys;
};

