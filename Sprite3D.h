#pragma once
#include "Headers.h"
#include "Util.h"
class Mesh;

class Sprite3D
{
public:
	Sprite3D();
	Sprite3D(const string& modelName, const string& textureName);
	~Sprite3D();

	glm::vec2 getPosition() const;
	void setPosition(const glm::vec2&);
	void setPosition(const float& x, const float& y);

	glm::vec2 getVelocity() const;
	void setVelocity(const glm::vec2&);
	void setVelocity(const float& x, const float& y);

	float getRenderingHeight() const;
	void setRenderingHeight(const float&);

	void setMesh(Mesh*);

	void draw();

	void setRotation(const glm::vec3& rotation, const float &degree);
	void setScale(const float&);

private:
	Mesh* mesh;

	glm::vec2 position;
	glm::vec2 velocity;

	float renderingHeight;
};

