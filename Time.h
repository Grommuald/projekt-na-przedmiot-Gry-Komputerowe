#pragma once
#include "Headers.h"

class Time
{
public:
	static const long long Second;
	static const long long Millisecond;

	static long long getTime();
	static double getDelta();
	static void setDelta(const double&);

private:
	static double delta;
};

class CountdownTimer {
public:
	CountdownTimer();

	void start(const long& seconds, function<void()> trigger);
	void reset();

	bool hasBeenSet() const { return _hasBeenSet; }
	bool hasBeenStopped() const { return _stopTimer; }

	unsigned long long getCurrentMillisecond() const { return currentMillisecond; }
private:
	unsigned long long currentMillisecond = 0ULL;
	unsigned long long elapsedTime;
	bool _hasBeenSet;
	bool _stopTimer;
};