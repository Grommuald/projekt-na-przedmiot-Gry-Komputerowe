#include "Shader.h"
#include "Log.h"
#include "Util.h"

Shader::Shader()
{
	uniforms = new std::unordered_map<std::string, int>();
}


Shader::~Shader()
{
	delete uniforms;

	glDetachShader(program, shader);
	glDeleteShader(shader);
}

void Shader::createProgram()
{
	if ((program = glCreateProgram()) == 0) {
		Log::logFatalError("Failed to create a shader.");
	}
}

void Shader::addVertexShader(const std::string& file)
{
	addProgram(file, GL_VERTEX_SHADER);
}

void Shader::addFragmentShader(const std::string& file)
{
	addProgram(file, GL_FRAGMENT_SHADER);
}

void Shader::addGeometryShader(const std::string& file)
{
	addProgram(file, GL_GEOMETRY_SHADER);
}

void Shader::addUniform(const std::string &uniform)
{
	int uniformLocation = glGetUniformLocation(program, uniform.c_str());
	if (uniformLocation == -1) {
		Log::logFatalError("Could not find uniform: " + uniform);
	}
	uniforms->insert({ uniform, uniformLocation });
}

void Shader::setUniform(const std::string &uniformName, const int & value)
{
	glUniform1i(uniforms->at(uniformName), value);
}

void Shader::setUniform(const std::string &uniformName, const float &value)
{
	glUniform1f(uniforms->at(uniformName), value);
}

void Shader::setUniform(const std::string &uniformName, const glm::vec3 &vec)
{
	glUniform3f(uniforms->at(uniformName), vec.x, vec.y, vec.z);
}

void Shader::setUniform(const std::string &uniformName, const glm::mat4& mat)
{
	glUniformMatrix4fv(uniforms->at(uniformName), 1, GL_FALSE, glm::value_ptr(mat));
}

void Shader::compileShader()
{
	glLinkProgram(program);

	GLint isLinked = 0;
	glGetProgramiv(program, GL_LINK_STATUS, &isLinked);
	if (isLinked == GL_FALSE) {
		Log::logError("Could not link the shader program.");
		showErrorInfo(program);
	}

	GLint isValidated = 0;
	glGetProgramiv(program, GL_VALIDATE_STATUS, &isValidated);
	if (isValidated == GL_FALSE) {
		Log::logError("Could not validate the shader program.");
		showErrorInfo(program);
	}
}

void Shader::bind()
{
	glUseProgram(program);
}

void Shader::addProgram(const std::string& file, const int& type)
{
	shader = glCreateShader(type);
	if (!shader) {
		Log::logFatalError("Could not create a shader: memory allocation");
	}
	if (!file.empty()) {
		const char* c_str = file.c_str();
		glShaderSource(shader, 1, &c_str, nullptr);
		glCompileShader(shader);

		GLint isCompiled = 0;
		glGetShaderiv(shader, GL_COMPILE_STATUS, &isCompiled);
		if (isCompiled == GL_FALSE) {
			Log::logError("Could not compile the shader program.");
			showErrorInfo(shader);
		}
		else {
			glAttachShader(program, shader);
		}
	}
	else {
		Log::logFatalError("Could not create a shader: couldn't load shader file");
	}
	
}


void Shader::showErrorInfo(const int &shader)
{
	GLint maxLength = 0;
	glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &maxLength);
	if (maxLength > 0) {
		std::vector<GLchar> errorLog(maxLength);
		glGetShaderInfoLog(shader, maxLength, &maxLength, &errorLog[0]);

		GLchar* info = new GLchar[maxLength];
		for (int i = 0; i < maxLength; ++i) {
			info[i] = errorLog[i];
		}
		Log::logFatalError(info);
	}
	else {
		Log::logError("Unknown shader error occured.");
	}

}
