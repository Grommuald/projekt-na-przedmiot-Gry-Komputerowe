#include "Doors.h"
#include "Sprite3D.h"
#include "Time.h"

Doors::Doors(const string& model_name, const string& texture_name, const  DungeonController::Location& location, const DoorType& type) : location(location), type(type),
	Entity(model_name, texture_name, "doors", glm::vec2(0, 0), glm::vec2(0, 0))
{
	gates = new Sprite3D("gates.obj", "stone_obstacle_texture.dds");
	gates->setScale(0.1f);

	sprite->setScale(0.1f);
	switch (location) {
	case  DungeonController::Location::Top:
		sprite->setRotation(glm::vec3(0, 0, 1), 90.0f);
		gates->setRotation(glm::vec3(0, 0, 1), 90.0f);
		setColliderOffset(0.0f, 0.2f);
		setPosition(0.0f, 1.92f);
		gates->setPosition(getPosition());
		setWidth(0.3f);
		setHeight(0.05f);
		break;
	case  DungeonController::Location::Bottom:
		sprite->setRotation(glm::vec3(0, 0, 1), 90.0f);
		gates->setRotation(glm::vec3(0, 0, 1), 90.0f);
		setPosition(0.0f, 0.13f);
		gates->setPosition(getPosition());
		setWidth(0.3f);
		setHeight(0.05f);
		break;
	case  DungeonController::Location::Left:
		setPosition(-1.87f, 1.05f);
		gates->setPosition(getPosition());
		setHeight(0.3f);
		setWidth(0.05f);
		break;
	case  DungeonController::Location::Right:
		setColliderOffset(0.2f, 0.0f);
		setPosition(1.87f, 1.05f);
		gates->setPosition(getPosition());
		setHeight(0.3f);
		setWidth(0.05f);
		break;
	}
}

Doors::~Doors()
{
	delete gates;
}

DungeonController::Location Doors::getLocation() {
	return location;
}

void Doors::draw()
{
	Entity::draw();
	gates->draw();
}

void Doors::moveDown()
{
	gates->setRenderingHeight(gates->getRenderingHeight() - 0.2f * Time::getDelta());
	if (gates->getRenderingHeight() <= -0.15f) {
		gates->setRenderingHeight(-0.15f);
	}
}
