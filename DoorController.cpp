#include "DoorController.h"
#include "Player.h"
#include "Room.h"
#include "Doors.h"
#include "CollisionController.h"
#include "Time.h"
#include "RoomController.h"

DoorController::DoorController(Player *player, Room *currentRoom, RoomController* roomController) 
	: Controller(player, currentRoom), roomController(roomController)
{
	gatesOpeningTimer = new CountdownTimer();
}

DoorController::~DoorController()
{
	delete gatesOpeningTimer;
}

void DoorController::draw()
{
	for (auto i : *(currentRoom->getDoors())) {
		i->draw();
	}
}

void DoorController::update()
{
	triggeredDoors = nullptr;
	
	if (doorsOpen) {
		if (!triggerGates) {
			gatesOpeningTimer->start(1000, [&]() {
				triggerGates = true;
				gatesOpeningTimer->reset();
			});
			auto i = currentRoom->getDoors()->begin();
			while (i != currentRoom->getDoors()->end()) {
				if ((*i)->notLocked()) {
					(*i)->moveDown();
				}
				i++;
			}
		}
		if (ifDoorsAvailable(DungeonController::Location::Left) &&
			player->getPosition().x <= -1.71f && player->getPosition().y >= 1.0f && player->getPosition().y <= 1.11f) {
			player->setCollidesWithDoors(true);
		}
		else if (ifDoorsAvailable(DungeonController::Location::Right) &&
			player->getPosition().x >= 1.80f && player->getPosition().y >= 1.0f && player->getPosition().y <= 1.11f) {
			player->setCollidesWithDoors(true);
		}
		else if (ifDoorsAvailable(DungeonController::Location::Top) &&
			player->getPosition().y >= 1.85f && player->getPosition().x >= -0.05f && player->getPosition().x <= 0.055f) {
			player->setCollidesWithDoors(true);
		}
		else if (ifDoorsAvailable(DungeonController::Location::Bottom) &&
			player->getPosition().y <= 0.20f && player->getPosition().x >= -0.05f && player->getPosition().x <= 0.055f) {
			player->setCollidesWithDoors(true);
		}
		else {
			player->setCollidesWithDoors(false);
		}
		auto i = currentRoom->getDoors()->begin();
		while (i != currentRoom->getDoors()->end()) {
			CollisionType collisionType = CollisionController::onCollision(*i, player);
			if (collisionType != CollisionType::NONE) {
				player->setCollidesWithDoors(true);
				triggeredDoors = *i;
			}
			++i;
		}
	}
	if (getTriggeredDoors() != nullptr) {
		player->setCanShoot(true);
		switch (getTriggeredDoors()->getLocation()) {
		case DungeonController::Location::Right:
			if (getTriggeredDoors()->getType() == Doors::DoorType::Boss) {
				if (player->hasObtainedKey()) {
					roomController->setCurrentRoom(player->getDungeonX() + 1, player->getDungeonY());
					player->setDungeonPosition(player->getDungeonX() + 1, player->getDungeonY());
					player->setTilePosition(1, 4);
				}
			}
			else {
				roomController->setCurrentRoom(player->getDungeonX() + 1, player->getDungeonY());
				player->setDungeonPosition(player->getDungeonX() + 1, player->getDungeonY());
				player->setTilePosition(1, 4);
			}
			break;
		case DungeonController::Location::Left:
			if (getTriggeredDoors()->getType() == Doors::DoorType::Boss) {
				if (player->hasObtainedKey()) {
					roomController->setCurrentRoom(player->getDungeonX() - 1, player->getDungeonY());
					player->setDungeonPosition(player->getDungeonX() - 1, player->getDungeonY());
					player->setTilePosition(17, 4);
				}
			}
			else {
				roomController->setCurrentRoom(player->getDungeonX() - 1, player->getDungeonY());
				player->setDungeonPosition(player->getDungeonX() - 1, player->getDungeonY());
				player->setTilePosition(17, 4);
			}
			break;
		case DungeonController::Location::Top:
			if (getTriggeredDoors()->getType() == Doors::DoorType::Boss) {
				if (player->hasObtainedKey()) {
					roomController->setCurrentRoom(player->getDungeonX(), player->getDungeonY() - 1);
					player->setDungeonPosition(player->getDungeonX(), player->getDungeonY() - 1);
					player->setTilePosition(9, 7);
				}
			}
			else {
				roomController->setCurrentRoom(player->getDungeonX(), player->getDungeonY() - 1);
				player->setDungeonPosition(player->getDungeonX(), player->getDungeonY() - 1);
				player->setTilePosition(9, 7);
			}
			break;
		case DungeonController::Location::Bottom:
			if (getTriggeredDoors()->getType() == Doors::DoorType::Boss) {
				if (player->hasObtainedKey()) {
					roomController->setCurrentRoom(player->getDungeonX(), player->getDungeonY() + 1);
					player->setDungeonPosition(player->getDungeonX(), player->getDungeonY() + 1);
					player->setTilePosition(9, 1);
				}
			}
			else {
				roomController->setCurrentRoom(player->getDungeonX(), player->getDungeonY() + 1);
				player->setDungeonPosition(player->getDungeonX(), player->getDungeonY() + 1);
				player->setTilePosition(9, 1);
			}
			break;
		}
	}

}

void DoorController::setDoorsOpen()
{
	doorsOpen = true;
}

void DoorController::setDoorsClosed()
{
	doorsOpen = false;
}

bool DoorController::ifDoorsAvailable(const DungeonController::Location &location)
{
	for (auto i : *(currentRoom->getDoors())) {
		if (i->getLocation() == location) {
			return true;
		}
	}
	return false;
}

Doors * DoorController::getTriggeredDoors()
{
	return triggeredDoors;
}

void DoorController::setCurrentRoom(Room *currentRoom)
{
	Controller::setCurrentRoom(currentRoom);
	triggerGates = false;
}
