#include "Bomb.h"
#include "Time.h"
#include "Sprite3D.h"
#include "Room.h"


Bomb::Bomb(const glm::vec2& position, const Tile& tile_position) : Entity("enemy.obj", "bomb_texture.dds", "bomb", position, glm::vec2(1, 0)), tileRadius(2)
{
	currentTilePosition.x = tile_position.x;
	currentTilePosition.y = tile_position.y;

	setPosition(position.x, position.y);

	explosionTimer = new CountdownTimer();
	sprite->setScale(0.07f);

	setWidth(0.1f);
	setHeight(0.1f);
}

Bomb::~Bomb()
{
	delete explosionTimer;
}

void Bomb::update()
{
	setPosition(getPosition());

	if (!doesExplode) {
		explosionTimer->start(1500, [&]() {
			doesExplode = true;
		});
	}
}

void Bomb::draw()
{
	Entity::draw();
}

void Bomb::updateTilePosition()
{
	currentTilePosition.x = static_cast<int>((getPosition().x + 0.1f + 1.80f) / 3.60f * 1000.0f) / 55;
	currentTilePosition.y = Room::ROOM_HEIGHT - static_cast<int>(((getPosition().y - 0.1f) / 1.60f)* 1000.0f) / 125 - 1;
}
