#include "Boss.h"
#include "Sprite3D.h"
#include "Text.h"

Boss::Boss(const int& x, const int& y) : Enemy("enemy.obj", "enemy_texture.dds", x, y)
{
	id = "boss";
	sprite->setScale(0.3f);
	setWidth(0.3f);
	setHeight(0.3f);

	health = 1.0f;
}


Boss::~Boss()
{
}
