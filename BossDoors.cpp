#include "BossDoors.h"


BossDoors::BossDoors(const DungeonController::Location& location) : 
	Doors("boss_doors.obj", "enemy_texture.dds", location, DoorType::Boss)
{
	locked = true;
}


BossDoors::~BossDoors()
{
}
