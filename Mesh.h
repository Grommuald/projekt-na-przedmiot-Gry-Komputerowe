#pragma once
#include "Util.h"
#include "Texture.h"

class Mesh
{
public:
	Mesh();
	~Mesh();

	void init();
	void draw();

	glm::vec3 getPosition() const { return position; }
	glm::vec4 getRotation() const { return rotation; }
	float getScale() const { return scale;}

	void setPosition(const glm::vec3& val) { position = val; }
	void setRotation(const glm::vec4& val) { rotation = val; }
	void setScale(const float& val) { scale = val; }

	Texture* const getTexture() const { return texture; }
	void setTexture(Texture* val) { texture = val; }

	
	vector<glm::vec3>  const getVertices() const { return vertices; }
	vector<glm::vec2>  const getTexcoords() const { return texcoords; }
	vector<glm::vec3>  const getNormals() const { return normals; }

	void setVertices(const vector<glm::vec3>& v) { vertices = v; }
	void setTexcoords(const vector<glm::vec2>&  t) { texcoords = t; }
	void setNormals(const vector<glm::vec3>& n) { normals = n; }

	void setTexcoordsPresence(const bool& val) { hasTexcoords = val; }
	void setNormalsPresence(const bool& val) { hasNormals = val; }
private:
	vector<glm::vec3> vertices;
	vector<glm::vec2> texcoords;
	vector<glm::vec3> normals;

	bool hasNormals;
	bool hasTexcoords;

	glm::vec3 color;
	glm::vec3 position;
	glm::vec4 rotation;

	float scale;

	Texture* texture;

	GLuint vertexbuffer;
	GLuint uvbuffer;
	GLuint normalbuffer;
};

