#include "ResourceLoader.h"
#include "Util.h"
#include "Log.h"
#include "Mesh.h"
#include "Texture.h"
#include "Room.h"

Mesh * ResourceLoader::loadMesh(const string & filename)
{
	if (Util::splitByCharacter(filename, '.')[1] != "obj") {
		Log::logFatalError("Invalid model format: " + filename);
	}
	string filePath = "./resource/models/" + filename;

	FILE * file = fopen(filePath.c_str(), "r");
	if (file == NULL) {
		Log::logFatalError("Could not open file: " + filename);
	}
	std::vector<unsigned int> vertexIndices, uvIndices, normalIndices;
	std::vector<glm::vec3> temp_vertices;
	std::vector<glm::vec2> temp_uvs;
	std::vector<glm::vec3> temp_normals;
	while (1) {

		char lineHeader[128];
		int res = fscanf(file, "%s", lineHeader);
		if (res == EOF)
			break; 

		if (strcmp(lineHeader, "v") == 0) {
			glm::vec3 vertex;
			fscanf(file, "%f %f %f\n", &vertex.x, &vertex.y, &vertex.z);
			temp_vertices.push_back(vertex);
		}
		else if (strcmp(lineHeader, "vt") == 0) {
			glm::vec2 uv;
			fscanf(file, "%f %f\n", &uv.x, &uv.y);
			uv.y = -uv.y; 
			temp_uvs.push_back(uv);
		}
		else if (strcmp(lineHeader, "vn") == 0) {
			glm::vec3 normal;
			fscanf(file, "%f %f %f\n", &normal.x, &normal.y, &normal.z);
			temp_normals.push_back(normal);
		}
		else if (strcmp(lineHeader, "f") == 0) {
			std::string vertex1, vertex2, vertex3;
			unsigned int vertexIndex[3], uvIndex[3], normalIndex[3];
			int matches = fscanf(file, "%d/%d/%d %d/%d/%d %d/%d/%d\n", &vertexIndex[0], &uvIndex[0], &normalIndex[0], &vertexIndex[1], &uvIndex[1], &normalIndex[1], &vertexIndex[2], &uvIndex[2], &normalIndex[2]);
			if (matches != 9) {
				Log::logFatalError("Could not read file: " + filename + " properly.");
				return false;
			}
			vertexIndices.push_back(vertexIndex[0]);
			vertexIndices.push_back(vertexIndex[1]);
			vertexIndices.push_back(vertexIndex[2]);
			uvIndices.push_back(uvIndex[0]);
			uvIndices.push_back(uvIndex[1]);
			uvIndices.push_back(uvIndex[2]);
			normalIndices.push_back(normalIndex[0]);
			normalIndices.push_back(normalIndex[1]);
			normalIndices.push_back(normalIndex[2]);
		}
		else {
			char stupidBuffer[1000];
			fgets(stupidBuffer, 1000, file);
		}

	}
	Mesh* newMesh = new Mesh();

	vector<glm::vec3> vertices, normals;
	vector<glm::vec2> texcoords;

	for (unsigned int i = 0; i<vertexIndices.size(); i++) {

		unsigned int vertexIndex = vertexIndices[i];
		unsigned int uvIndex = uvIndices[i];
		unsigned int normalIndex = normalIndices[i];

		glm::vec3 vertex = temp_vertices[vertexIndex - 1];
		glm::vec2 uv = temp_uvs[uvIndex - 1];
		glm::vec3 normal = temp_normals[normalIndex - 1];

		vertices.push_back(vertex);
		texcoords.push_back(uv);
		normals.push_back(normal);

	}
	newMesh->setVertices(vertices);

	newMesh->setTexcoordsPresence(true);
	newMesh->setTexcoords(texcoords);

	newMesh->setNormalsPresence(true);
	newMesh->setNormals(normals);

	return newMesh;
}

Texture * ResourceLoader::loadTexture(const string & filename)
{
	string ext = Util::splitByCharacter(filename, '.')[1];
	Texture *handle = nullptr;

	if (ext == "bmp") {
		handle = loadBMP(filename);
	}
	else if (ext == "dds") {
		handle = loadDDS(filename);
	}
	else {
		Log::logFatalError("Invalid texture format: " + filename);
		return nullptr;
	}
	return handle;
}
#define FOURCC_DXT1 0x31545844 // Equivalent to "DXT1" in ASCII
#define FOURCC_DXT3 0x33545844 // Equivalent to "DXT3" in ASCII
#define FOURCC_DXT5 0x35545844 // Equivalent to "DXT5" in ASCII

Texture * ResourceLoader::loadBMP(const string & filename)
{
	string fullPath = "./resource/textures/" + filename;

	unsigned char header[54];
	unsigned int dataPos;
	unsigned int imageSize;
	unsigned int width, height;
	// Actual RGB data
	unsigned char * data;

	// Open the file
	FILE * file = fopen(fullPath.c_str(), "rb");
	if (!file) {
		Log::logFatalError("Could not load bmp file: " + filename);
	}

	// If less than 54 bytes are read, problem
	if (fread(header, 1, 54, file) != 54) {
		Log::logFatalError("Could not load bmp file: " + filename);
	}
	// A BMP files always begins with "BM"
	if (header[0] != 'B' || header[1] != 'M') {
		Log::logFatalError("Could not load bmp file: " + filename);
	}
	// Make sure this is a 24bpp file
	if (*(int*)&(header[0x1E]) != 0) {
		Log::logFatalError("Could not load bmp file: " + filename);
	}
	if (*(int*)&(header[0x1C]) != 24) {
		Log::logFatalError("Could not load bmp file: " + filename);
	}

	dataPos = *(int*)&(header[0x0A]);
	imageSize = *(int*)&(header[0x22]);
	width = *(int*)&(header[0x12]);
	height = *(int*)&(header[0x16]);


	if (imageSize == 0)    imageSize = width*height * 3; 
	if (dataPos == 0)      dataPos = 54; 

										
	data = new unsigned char[imageSize];

	fread(data, 1, imageSize, file);

	fclose(file);

	unsigned char* dataReversed = new unsigned char[imageSize];

	for (int i = imageSize - 1; i >= 0; i -= 3) {
		int index = imageSize - i;
		dataReversed[i - 2] = data[index - 1];
		dataReversed[i - 1] = data[index + 0];
		dataReversed[i - 0] = data[index + 1];
	}

	GLuint textureID;
	glGenTextures(1, &textureID);

	glBindTexture(GL_TEXTURE_2D, textureID);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_BGR, GL_UNSIGNED_BYTE, dataReversed);

	delete[] data;
	delete[] dataReversed;

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glGenerateMipmap(GL_TEXTURE_2D);

	Texture* newTexture = new Texture(textureID);
	return newTexture;
}

Texture* ResourceLoader::loadDDS(const string& filename) {

	unsigned char header[124];
	string fullPath = "./resource/textures/" + filename;
	FILE *fp;

	fp = fopen(fullPath.c_str(), "rb");
	if (fp == NULL) {
		Log::logFatalError("Could not open file: " + fullPath);
		return 0;
	}

	char filecode[4];
	fread(filecode, 1, 4, fp);
	if (strncmp(filecode, "DDS ", 4) != 0) {
		fclose(fp);
		return 0;
	}

	fread(&header, 124, 1, fp);

	unsigned int height = *(unsigned int*)&(header[8]);
	unsigned int width = *(unsigned int*)&(header[12]);
	unsigned int linearSize = *(unsigned int*)&(header[16]);
	unsigned int mipMapCount = *(unsigned int*)&(header[24]);
	unsigned int fourCC = *(unsigned int*)&(header[80]);

	unsigned char * buffer;
	unsigned int bufsize;

	bufsize = mipMapCount > 1 ? linearSize * 2 : linearSize;
	buffer = (unsigned char*)malloc(bufsize * sizeof(unsigned char));
	fread(buffer, 1, bufsize, fp);

	fclose(fp);

	unsigned int components = (fourCC == FOURCC_DXT1) ? 3 : 4;
	unsigned int format;
	switch (fourCC)
	{
	case FOURCC_DXT1:
		format = GL_COMPRESSED_RGBA_S3TC_DXT1_EXT;
		break;
	case FOURCC_DXT3:
		format = GL_COMPRESSED_RGBA_S3TC_DXT3_EXT;
		break;
	case FOURCC_DXT5:
		format = GL_COMPRESSED_RGBA_S3TC_DXT5_EXT;
		break;
	default:
		free(buffer);
		return 0;
	}

	GLuint textureID;
	glGenTextures(1, &textureID);

	glBindTexture(GL_TEXTURE_2D, textureID);
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

	unsigned int blockSize = (format == GL_COMPRESSED_RGBA_S3TC_DXT1_EXT) ? 8 : 16;
	unsigned int offset = 0;

	for (unsigned int level = 0; level < mipMapCount && (width || height); ++level)
	{
		unsigned int size = ((width + 3) / 4)*((height + 3) / 4)*blockSize;
		glCompressedTexImage2D(GL_TEXTURE_2D, level, format, width, height,
			0, size, buffer + offset);

		offset += size;
		width /= 2;
		height /= 2;

		if (width < 1) width = 1;
		if (height < 1) height = 1;

	}

	free(buffer);

	Texture* newTexture = new Texture(textureID);
	return newTexture;
}
string ResourceLoader::loadShader(const string & filename)
{
	std::stringstream shaderSource;
	std::string fullPath = "./resource/shaders/" + filename;
	std::ifstream shaderReader(fullPath.c_str());

	size_t linesNumber = 0U;

	if (shaderReader.is_open()) {
		std::string line;
		while (std::getline(shaderReader, line)) {
			shaderSource << line << "\n";
		}
		puts(shaderSource.str().c_str());
	}
	else {
		Log::logFatalError("Couldn't load \"" + filename + "\" shader.");
	}
	shaderReader.close();
	return shaderSource.str().c_str();
}

short * ResourceLoader::loadRoomPattern(const string & filename)
{
	string filePath = "./resource/room_patterns/" + filename;
	ifstream inputFile(filePath, ios::in);
	if (!inputFile.is_open()) {
		inputFile.close();
		Log::logFatalError("Could not open pattern file: " + filename);
	}

	short* result = new short[Room::ROOM_WIDTH * Room::ROOM_HEIGHT];
	size_t i = 0;
	while (inputFile >> result[i++])
			;
	for (i = 0; i < Room::ROOM_WIDTH * Room::ROOM_HEIGHT; ++i) {
		if (Room::ROOM_WIDTH % 19 == 0) {
			cout << endl;
		}
		cout << result[i] << " ";
	}

	inputFile.close();
	return result;
}
