#pragma once
#include "Controller.h"
#include "Room.h"

class AIController;
class ObstacleController;
class ItemController;
class DoorController;
class ShotController;
class DungeonController;
class ControllerObserver;
class BombController;
class KeyController;
class BossController;
class FadingCaption;
class CountdownTimer;

class RoomController : public Controller
{
public:
	RoomController(Player*);
	~RoomController();

	void update();
	void draw();
	void drawFadingScreen();

	Room* generateRoom(const size_t& dungeon_x, const size_t& dungeon_y);
	Room* generateStartingRoom(const size_t& dungeon_x, const size_t& dungeon_y);
	Room* generateKeyRoom(const size_t& dungeon_x, const size_t& dungeon_y);
	Room* generateBossRoom(const size_t& dungeon_x, const size_t& dungeon_y);

	bool getEndCondition() const;

	void restart();
	void setCurrentRoom(const int& dungeon_x, const int& dungeon_y);
private:
	void addDoors(Room*);
	
	vector<Room*>* rooms;

	FadingCaption* playerDeadCaption;
	FadingCaption* bossSlainCaption;

	ShotController* shotController;
	AIController* aiController;
	ObstacleController* obstacleController;
	ItemController* itemController;
	DoorController* doorController;
	DungeonController* dungeonController;
	BombController* bombController;
	KeyController* keyController;
	BossController* bossController;

	ControllerObserver* controllersObserver;

	static const unsigned short NUMBER_OF_PATTERNS;
};

