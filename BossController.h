#pragma once
#include "Controller.h"

class Boss;
class CountdownTimer;
class Shot;

class BossController :
	public Controller
{
public:
	BossController(Player*, Boss*, Room*);
	~BossController();

	void update() final;
	void draw() final;

	void setBossSlain(const bool& val) { bossSlain = val; }
	bool isBossSlain() const { return bossSlain; }
private:
	void spawnShot(const glm::vec2& position, const glm::vec2& direction, const float& speed, const float& damage);
	void jumpTo(const float& x, const float& y);

	Boss* boss;
	CountdownTimer* shotTimer;
	CountdownTimer* moveTimer;

	bool bossSlain = false;

	bool canShoot;
	bool canMove;

	bool jumpNotDone;
	bool startedJump;
	bool endedJump;

	glm::vec2 jumpStartingPosition;
	glm::vec2 jumpDestination;
	glm::vec2 shootingDirection;
	glm::vec2 jumpDirection;

	vector<Shot*> bullets;
};

