#pragma once
#include "Headers.h"
class Texture;

class Text
{
public:
	static void initialize(Texture*);
	static void initialize();
	static void dispose();
	static void show(const string&, int, int, const int&);
private:
	static GLuint vertexbuffer;
	static GLuint uvbuffer;
	static GLuint vao;

	static Texture* texture;

	struct Character {
		GLuint TextureID;
		glm::ivec2 Size;       // Size of glyph
		glm::ivec2 Bearing;    // Offset from baseline to left/top of glyph
		GLuint     Advance;    // Offset to advance to next glyph
	};

	static map<GLchar, Character> Characters;
	static GLuint textureId;

};

